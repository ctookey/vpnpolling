﻿Imports System.Management
Imports System.ServiceProcess
Imports System.Threading

Public Class CheckVirusScanner
    Private Shared DefScanThread As New Thread(New ThreadStart(AddressOf StartVSScanDef))
    Private Shared DefDBThread As New Thread(New ThreadStart(AddressOf StartDBUpdateDef))

    Private Shared VSScanPid As Integer = 0
    Private Shared VSDBPid As Integer = 0

    Public Shared Sub CheckVSDatabase()
        Try
            If CheckVSRegDB() = True Then

                Dim Date1Str As String = "01/01/2015"

                If VPNPolling.VSUsed = "Defender" Then
                    'Testing Windows Defender Updates date
                    If System.Environment.OSVersion.VersionString.Contains("Windows NT 6.2") Then
                        If ChkDefenderSrv() Then
                            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Defender\Signature Updates", "AVSignatureApplied", Nothing) Is Nothing Then
                                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Defender\Signature Updates", "AVSignatureApplied", "")
                                VPNPolling.WDUpdateDate = "Empty"
                            Else
                                LogFile.WriteEntry("Reading Windows 10 Defender value")
                                Dim RegVal = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Defender\Signature Updates", "AVSignatureApplied", "Empty")
                                VPNPolling.WDUpdateDate = DateTime.FromFileTime(BitConverter.ToInt64(RegVal, 0))
                                LogFile.WriteEntry("Windows 10 Defender DB date : " & VPNPolling.WDUpdateDate)
                            End If
                        Else
                            VPNPolling.MessString = "Unable to read VS DB, Stopping VPNs from starting"
                            VPN.Kill("Default")
                        End If
                    End If
                    Date1Str = VPNPolling.WDUpdateDate
                End If

                'Setup dates in the same format without the time element, and in UK format
                Dim Date2str As String = DateTime.Now.ToShortDateString

                'Convert to Date format
                Dim datTim1 As Date
                Dim datTim2 As Date

                'Makes sure the value read from registry is a valid format date
                Try
                    datTim1 = Date.Parse(Date1Str).ToShortDateString
                Catch ex As Exception
                    LogFile.WriteEntry("Error unable to read reg value unable to determine date of last DB update, setting date to 2015/01/01")
                    datTim1 = Date.Parse("2015/01/01")
                End Try
                datTim2 = Date.Parse(Date2str)

                LogFile.WriteEntry("VS Database date : " & datTim1)
                LogFile.WriteEntry("Current date : " & datTim2)
                Dim datdif As Integer = DateDiff(DateInterval.Day, datTim1, datTim2)
                Dim Totday As Integer = VPNPolling.DBDays - datdif
                Dim StrBuild As String = VPNPolling.ReportGen(Totday)
                LogFile.WriteEntry("Days different = " & datdif.ToString & " <= " & VPNPolling.DBDays)
                LogFile.WriteEntry(StrBuild & " DB update, database age " & datTim1)
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "DBDiffDate", StrBuild)

                'Checking defender DB age
                If datdif > VPNPolling.DBDays Or VPNPolling.DBDays = 0 Then
                    LogFile.WriteEntry("Virus database out of date - stopping VPNs from starting")
                    VPNPolling.MessString = "Virus database out of date - Stopping VPNs from starting"
                    VPNPolling.ProblemIconAlert = True
                    VPN.Kill("Default")

                    If VPNPolling.VSUsed = "Defender" And Not DefDBThread.IsAlive Then
                        DefDBThread = New Thread(New ThreadStart(AddressOf StartDBUpdateDef))
                        DefDBThread.IsBackground = True
                        DefDBThread.Start() 'Start Def DB update if out of date and enabled
                    End If
                Else
                    LogFile.WriteEntry("VS Database up to date")
                    VPNPolling.DBupdateTries = 0 'Clear DBTries as system is clear (StartDBUpdate)
                End If
            Else
                VPNPolling.ProblemIconAlert = True
                VPNPolling.MessString = "Unable to read VS DB, Stopping VPNs from starting"
                LogFile.WriteEntry("Unable to read VS DB, Stopping VPNs from starting")
                VPN.Kill("Default")
            End If
        Catch ex As Exception
            LogFile.WriteEntry("PollingService encountered an error '" & ex.Message & "'")
            LogFile.WriteEntry("PollingService service Stack Trace: " & ex.StackTrace)
            VPN.Kill("Default")
        End Try
    End Sub

    Public Shared Sub CheckVSScan()
        Try
            If CheckVSRegScan() = True Then

                Dim Date1str As String = "01/01/2015"

                If VPNPolling.VSUsed = "Defender" Then
                    'Testing Windows Defender Updates date
                    If System.Environment.OSVersion.VersionString.Contains("Windows NT 6.2") Then
                        If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Defender\Scan", "LastScanRun", Nothing) Is Nothing Then
                            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Defender\Scan", "LastScanRun", "")
                            VPNPolling.WDUpdateDate = DateTime.FromFileTime("01/01/2020")
                        Else
                            LogFile.WriteEntry("Reading Windows 10 Defender Scan date Value")
                            Dim RegVal = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Defender\Scan", "LastScanRun", "Empty")
                            VPNPolling.WDUpdateDate = DateTime.FromFileTime(BitConverter.ToInt64(RegVal, 0))
                            LogFile.WriteEntry("Windows Defender scan date : " & VPNPolling.WDUpdateDate)
                        End If
                    End If
                    Date1str = VPNPolling.WDUpdateDate
                End If

                'Setup dates in the same format without the time element, and in UK format
                Dim Date2str As String = DateTime.Now.ToShortDateString

                'Convert to Date format
                Dim datTim1 As Date = Date.Parse(Date1str).ToShortDateString
                Dim datTim2 As Date = Date.Parse(Date2str)

                LogFile.WriteEntry("VS Scan date : " & datTim1)
                LogFile.WriteEntry("Current date : " & datTim2)
                Dim datdif As Integer = DateDiff(DateInterval.Day, datTim1, datTim2)
                Dim Totday As Integer = VPNPolling.ScanDays - datdif
                Dim StrBuild As String = VPNPolling.ReportGen(Totday)
                LogFile.WriteEntry("Days different = " & datdif.ToString & " <= " & VPNPolling.ScanDays)
                LogFile.WriteEntry(StrBuild & " system quickscan")
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "ScanDiffDate", StrBuild)

                'Checking Scan start
                If datdif > VPNPolling.ScanDays Or VPNPolling.ScanDays = 0 Then
                    LogFile.WriteEntry("Virus scan out of date - Stopping VPNs from starting")
                    VPNPolling.MessString = "Virus scan out of date - Stopping VPNs from starting"
                    VPNPolling.ProblemIconAlert = True
                    VPN.Kill("Default")

                    'Changes scan type depending on date difference
                    If datdif < 30 Then
                        'Quick scan
                        LogFile.WriteEntry("Scan type 1 (Quick scan) configured")
                        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "ScanType", "1")
                    Else
                        'Full scan
                        LogFile.WriteEntry("Scan type 2 (Full system scan) configured")
                        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "ScanType", "2")
                    End If

                    If VPNPolling.VSUsed = "Defender" And Not DefScanThread.IsAlive Then
                        DefScanThread = New Thread(New ThreadStart(AddressOf StartVSScanDef))
                        DefScanThread.IsBackground = True
                        DefScanThread.Start() 'Start Defender scan if out of date and enabled
                    End If
                Else
                    LogFile.WriteEntry("VS Scan up to date")
                End If
            Else
                VPNPolling.ProblemIconAlert = True
                VPNPolling.MessString = "Unable to read VS Scan, Stopping VPNs from starting"
                LogFile.WriteEntry("Unable to read VS Scan, Stopping VPNs from starting")
                VPN.Kill("Default")
            End If
        Catch ex As Exception
            LogFile.WriteEntry("PollingService encountered an error '" & ex.Message & "'")
            LogFile.WriteEntry("PollingService service Stack Trace: " & ex.StackTrace)
            VPN.Kill("Default")
        End Try
    End Sub

    Private Shared Function CheckVSRegScan()
        If VPNPolling.VSUsed = "Defender" Then
            Dim N3VMVersion As String = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation\", "Model", "")
            If N3VMVersion.Contains("VPN v5") Then
                Try
                    Dim RegVal = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Defender\Scan", "LastScanRun", "Empty")
                    VPNPolling.WDUpdateDate = DateTime.FromFileTime(BitConverter.ToInt64(RegVal, 0))
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "AVScanDate", Date.Parse(VPNPolling.WDUpdateDate).ToShortDateString)
                    If VPNPolling.PTrace = True Then
                        LogFile.WriteEntry("Reading scan as : " & VPNPolling.WDUpdateDate)
                        LogFile.WriteEntry("Writing AVScan to regsitry " & Date.Parse(VPNPolling.WDUpdateDate).ToShortDateString)
                    End If
                    Return True
                Catch
                    LogFile.WriteEntry("Unable to get AVScan to regsitry so setting default and running scan")
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "AVScanDate", "01/01/2019")
                    StartVSScanDef()
                    Return True
                End Try
            End If
        End If
        Return (False)
    End Function

    Private Shared Function CheckVSRegDB()
        If VPNPolling.VSUsed = "Defender" Then

            'Record Defender DB date into readable string
            Dim N3VMVersion As String = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation\", "Model", "")
            If N3VMVersion.Contains("VPN v5") Then
                Try
                    Dim RegVal = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Defender\Signature Updates", "AVSignatureApplied", "Empty")
                    VPNPolling.WDUpdateDate = DateTime.FromFileTime(BitConverter.ToInt64(RegVal, 0))
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "AVDBDate", Date.Parse(VPNPolling.WDUpdateDate).ToShortDateString)
                    If VPNPolling.PTrace = True Then LogFile.WriteEntry("Writing AVDB to regsitry " & Date.Parse(VPNPolling.WDUpdateDate).ToShortDateString)
                    Return True
                Catch ex As Exception '@TODO
                    LogFile.WriteEntry("Unable to get AVDB to regsitry so setting default and running scan")
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "AVDBDate", "01/01/2019")
                    StartDBUpdateDef()
                    Return True
                End Try
            End If
        End If
        Return False
    End Function

    Private Shared Function ChkDefenderSrv() As Boolean
        'Checking status of Microsoft AV as a service
        Dim servicesButNotDevices As ServiceController() = ServiceController.GetServices()
        Dim ServiceExists As Boolean = False
        Dim DefSrv = "WinDefend"

        For Each services As ServiceController In servicesButNotDevices
            If services.ServiceName = DefSrv Then 'May also use DispalyName property depending on your use case
                ServiceExists = True
                Exit For
            End If
        Next

        If ServiceExists = True Then
            Dim service As ServiceController = New ServiceController(DefSrv)
            'Check service state
            If Not service.Status.Equals(ServiceControllerStatus.Running) Then
                LogFile.WriteEntry("Attempting to start '" & service.DisplayName & "' as service is not running")
                service.Start()
                Return False
            End If
            If VPNPolling.PTrace = True Then LogFile.WriteEntry("Windows Service: " & service.DisplayName & " is running") 'Only writes to log if tracing is turned on
            Return True
        End If
        If VPNPolling.PTrace = True Then LogFile.WriteEntry("Unable to determine Microsoft defender state") 'Only writes to log if tracing is turned on
        Return False
    End Function

    Public Shared Sub StartVSScanDef()
        Try
            If System.IO.File.Exists("C:\Program Files\Windows Defender\MpCmdRun.exe") Then
                'Searching if scan is already running
                For Each prog As Process In Process.GetProcesses
                    If prog.ProcessName = "MpCmdRun" Then
                        Dim searcher As New ManagementObjectSearcher("SELECT CommandLine FROM Win32_Process WHERE ProcessId = " + prog.Id.ToString)
                        Dim cmdline As String = ""
                        For Each progfound As ManagementObject In searcher.Get()
                            cmdline = progfound("CommandLine")
                        Next
                        If cmdline.Contains("-Scan") Then
                            WriteResultsReg("VSAmt") 'Recording the time it takes to download updates
                            LogFile.WriteEntry("$$$ - Defender quickscan already in progress")
                            Exit Sub
                        End If
                    End If
                Next
                'Scanning initated by the ICNETCheck appliaction for correct scanning features
                LogFile.WriteEntry("$$$ - Starting Defender quickscan . . . .")
                WriteResultsReg("VSAmt")
                Networking.MessageOut("Defender Quickscan in progress")
            Else
                LogFile.WriteEntry("ERROR - Defender MpCmdRun.exe missing unable to VScan update autostart")
            End If
        Catch ex As Exception
            LogFile.WriteEntry("PollingService VSScan encountered an error '" & ex.Message & "'")
            LogFile.WriteEntry("PollingService VSScan service stack trace: " & ex.StackTrace)
        End Try
    End Sub

    'Starts Defender DB updates if the internet server listed is available
    Public Shared Sub StartDBUpdateDef()
        Try
            If System.IO.File.Exists("C:\Program Files\Windows Defender\MpCmdRun.exe") And VPNPolling.DBupdateTries <= 5 Then
                Try
                    If Networking.CheckForInternetConnection() Then ' Checks if the internet is avilable to get Defender updates
                        'Searching if scan is already running
                        For Each prog As Process In Process.GetProcesses
                            If prog.ProcessName = "MpCmdRun" Then
                                Dim searcher As New ManagementObjectSearcher("SELECT CommandLine FROM Win32_Process WHERE ProcessId = " + prog.Id.ToString)
                                Dim cmdline As String = ""
                                For Each progfound As ManagementObject In searcher.Get()
                                    cmdline = progfound("CommandLine")
                                Next
                                If cmdline.Contains("-SignatureUpdate") Then
                                    WriteResultsReg("VSDBSecInt") 'Recording the time it takes to download updates
                                    LogFile.WriteEntry("$$$ - Defender DB update already in progress")
                                    VPNPolling.DBupdateTries = 0
                                    Exit Sub
                                End If
                            End If
                        Next

                        WriteResultsReg("DBUptAmt") 'Setting registry with DBupdate amount for results
                        WriteResultsReg("VSDBSecInt") 'Recording the time it takes to download Defender updates
                        LogFile.WriteEntry("$$$ - Starting Defender DB update . . . .")
                        'Scanning initated by the ICNETCheck appliaction for correct scanning features
                        Networking.MessageOut("Defender security update in progress")
                        VPNPolling.DBupdateTries += 1
                    Else
                        LogFile.WriteEntry("Unable to start Defender DB update as not connected to internet, unable to resolve host")
                    End If
                Catch ex As Exception
                    LogFile.WriteEntry("$$$ - Unable to start Defender DB update as not connected to internet, posssible VM network interface issues. Unable to resolve")
                End Try
            ElseIf VPNPolling.DBupdateTries > 5 Then
                LogFile.WriteEntry("$$$ - Defender DB autostart failing, please start manually")
            Else
                LogFile.WriteEntry("ERROR - Defender MpCmdRun.exe missing unable to DB update autostart")
            End If
        Catch ex As Exception
            LogFile.WriteEntry("PollingService VDBUpdate encountered an error '" &
                    ex.Message & "'")
            LogFile.WriteEntry("PollingService VDBUpdate service Stack Trace: " &
                ex.StackTrace)
        End Try
    End Sub

    Public Shared Sub AbortThreads()
        If DefScanThread.IsAlive Then DefScanThread.Abort()
        If DefDBThread.IsAlive Then DefDBThread.Abort()
    End Sub
End Class
