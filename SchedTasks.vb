﻿Imports Microsoft.Win32.TaskScheduler
Public Class SchedTasks

    Public Shared Function CreateTasks()
        AutoUpdateSched()
        IcnetStartSched()
        BGinfoSched()
        PollerSched()
        N3VMUpdateSched()
        N3TaskbarPinSched()
    End Function

    'Create Poller Autoupdate Sched Task
    Private Shared Sub AutoUpdateSched()
        Using ts As New TaskService()

            Dim TaskNames As TaskFolder = ts.RootFolder
            For Each t As Task In TaskNames.Tasks
                If t.Name = "ICNET Poller Autoupdate" Then
                    Try
                        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SchTasks", "ICNET Poller Autoupdate", 1)
                    Catch ex As Exception
                        LogFile.WriteEntry(ex.Message)
                        LogFile.WriteEntry(ex.StackTrace)
                    End Try
                    Exit Sub
                End If
            Next

            LogFile.WriteEntry("Task not found, creating autoupdate task for windows Task Scheduler")

            ' Create a new task definition and assign properties
            Dim td As TaskDefinition = ts.NewTask()
            td.RegistrationInfo.Description = "Automatic version control\updating for ICNET Checker"

            ' Create a trigger that will fire the task at this time every day with a randomization of hours
            Dim SetTime As Date = Date.Now.ToShortDateString & " " & VPNPolling.AuSchedTime
            Dim RandomSet As TimeSpan = TimeSpan.FromHours(VPNPolling.AuSchedHr)
            td.Triggers.Add(New DailyTrigger() With {.DaysInterval = VPNPolling.AuDaysInt, .StartBoundary = SetTime, .RandomDelay = RandomSet})

            ' Create an action that will launch Notepad whenever the trigger fires
            td.Actions.Add(New ExecAction("""" & PollerLoc & "\AutoPoller.exe""", , PollerLoc))
            td.Principal.RunLevel = TaskRunLevel.Highest
            td.Settings.Hidden = True
            td.Settings.AllowDemandStart = True
            td.Settings.StartWhenAvailable = True
            td.Settings.DisallowStartIfOnBatteries = False

            ' Register the task in the root folder
            Try
                ts.RootFolder.RegisterTaskDefinition("ICNET Poller Autoupdate", td)
            Catch
                LogFile.WriteEntry("ICNET Poller autoupdate task might already exist")
            End Try
        End Using
    End Sub

    'Create ICNet Start Sched Task
    Private Shared Sub IcnetStartSched()
        Using ts As New TaskService()

            Dim TaskNames As TaskFolder = ts.RootFolder
            For Each t As Task In TaskNames.Tasks
                If t.Name = "ICNET Start" Then
                    Try
                        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SchTasks", "ICNET Start", 1)
                    Catch ex As Exception
                        LogFile.WriteEntry(ex.Message)
                        LogFile.WriteEntry(ex.StackTrace)
                    End Try
                    Exit Sub
                End If
            Next

            LogFile.WriteEntry("Task not found, creating ICNET Start task for windows Task Scheduler")

            ' Create a new task definition and assign properties
            Dim td As TaskDefinition = ts.NewTask()
            td.RegistrationInfo.Description = "Automatic version control\updating for ICNET Checker"

            ' Create an action that will launch Notepad whenever the trigger fires
            td.Actions.Add(New ExecAction("""" & PollerLoc & "\ICNetCheck.exe""", , PollerLoc))
            td.Triggers.AddNew(TaskTriggerType.Logon)
            td.Principal.RunLevel = TaskRunLevel.Highest
            td.Settings.Hidden = True
            td.Settings.AllowDemandStart = True
            td.Settings.StartWhenAvailable = True
            td.Settings.AllowHardTerminate = False
            td.Settings.DisallowStartIfOnBatteries = False
            Dim ExcTime As TimeSpan = TimeSpan.FromHours(0)
            td.Settings.ExecutionTimeLimit = ExcTime

            ' Register the task in the root folder
            Try
                ts.RootFolder.RegisterTaskDefinition("ICNET Start", td, TaskCreation.Create, "VPN User", "KeySecurity42", TaskLogonType.InteractiveToken)
            Catch
                LogFile.WriteEntry("ICNET Start task might already exist")
            End Try
        End Using
    End Sub

    'Create BGInfo Sched Task
    Private Shared Sub BGinfoSched()
        Using ts As New TaskService()

            Dim TaskNames As TaskFolder = ts.RootFolder
            For Each t As Task In TaskNames.Tasks
                If t.Name = "BGinfo Refresh" Then
                    Try
                        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SchTasks", "BGinfo Refresh", 1)
                    Catch ex As Exception
                        LogFile.WriteEntry(ex.Message)
                        LogFile.WriteEntry(ex.StackTrace)
                    End Try
                    Exit Sub
                End If
            Next

            LogFile.WriteEntry("Task not found, creating BGinfo Refresh task for windows Task Scheduler")

            ' Create a new task definition and assign properties
            Dim td As TaskDefinition = ts.NewTask()
            td.RegistrationInfo.Description = "BGinfo Refresh Task"

            ' Create a trigger that will fire the task at this time every day with a randomization of hours
            Dim tsk As Trigger = New DailyTrigger
            Dim SetTime As Date = Date.Now.ToShortDateString & " 08:00:00"
            tsk.StartBoundary = SetTime
            tsk.Repetition.Interval = TimeSpan.FromHours(1)
            td.Triggers.Add(tsk)
            'td.Triggers.AddNew(TaskTriggerType.Logon)

            ' Create an action that will launch Notepad whenever the trigger fires
            td.Actions.Add(New ExecAction("C:\Program Files\BGinfo\BGinfo.exe", "C:\N3-IT\BGinfo\N3info.bgi /timer:0", "C:\Program Files\BGinfo"))
            td.Principal.RunLevel = TaskRunLevel.Highest
            td.Settings.Hidden = False
            td.Settings.AllowDemandStart = True
            td.Settings.StartWhenAvailable = True
            td.Settings.AllowHardTerminate = False
            td.Settings.DisallowStartIfOnBatteries = False
            Dim ExcTime As TimeSpan = TimeSpan.FromHours(0)
            td.Settings.ExecutionTimeLimit = ExcTime

            ' Register the task in the root folder
            Try
                ts.RootFolder.RegisterTaskDefinition("BGinfo Refresh", td, TaskCreation.Create, "VPN User", "KeySecurity42", TaskLogonType.InteractiveToken)
            Catch
                LogFile.WriteEntry("BGinfo Refresh task might already exist")
            End Try
        End Using
    End Sub

    'Create Keep PollerRunning Sched Task
    Private Shared Sub PollerSched()
        Using ts As New TaskService()

            Try
                Try
                    Dim TaskNames As TaskFolder = ts.GetFolder("\MMC")
                    For Each t As Task In TaskNames.Tasks
                        If t.Name = "Keep Running" Then
                            Try
                                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SchTasks", "Keep Running", 1)
                            Catch ex As Exception
                                LogFile.WriteEntry(ex.Message)
                                LogFile.WriteEntry(ex.StackTrace)
                            End Try
                            Exit Sub
                        End If
                    Next
                Catch
                    'Create new MMC Task folder of ICNET keep running task
                    ts.RootFolder.CreateFolder("\MMC")
                End Try

                LogFile.WriteEntry("Task not found, creating keep running task for windows Task Scheduler")

                ' Create a new task definition and assign properties
                Dim td As TaskDefinition = ts.NewTask()
                td.RegistrationInfo.Description = "Keep Running"

                ' Create a trigger that will fire the task at this time every day with a randomization of hours
                Dim tsk As Trigger = New DailyTrigger
                Dim SetTime As Date = Date.Now.ToShortDateString & " 08:00:00"
                tsk.StartBoundary = SetTime
                tsk.Repetition.Interval = TimeSpan.FromMinutes(1)
                td.Triggers.Add(tsk)

                ' Create an action that will launch Notepad whenever the trigger fires @TODO
                td.Actions.Add(New ExecAction("Net", "start pollingservice"))
                td.Principal.RunLevel = TaskRunLevel.Highest
                td.Settings.Hidden = False
                td.Settings.AllowDemandStart = True
                td.Settings.StartWhenAvailable = True
                td.Settings.AllowHardTerminate = False
                td.Settings.DisallowStartIfOnBatteries = False
                Dim ExcTime As TimeSpan = TimeSpan.FromHours(0)
                td.Settings.ExecutionTimeLimit = ExcTime

                ' Register the task in the root folder
                Try
                    ts.RootFolder.RegisterTaskDefinition("\MMC\Keep Running", td)
                Catch ex As Exception
                    LogFile.WriteEntry("Keep running task might already exist")
                    LogFile.WriteEntry(ex.Message)
                    LogFile.WriteEntry(ex.StackTrace)
                End Try
            Catch
                LogFile.WriteEntry("Keep running task Nothing to do")
            End Try

        End Using
    End Sub

    'Create Poller N3 VPN Update Sched Task
    Private Shared Sub N3VMUpdateSched()
        Using ts As New TaskService()

            Dim TaskNames As TaskFolder = ts.RootFolder
            For Each t As Task In TaskNames.Tasks
                If t.Name = "N3 VM Updater" Then
                    Try
                        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SchTasks", "N3 VM Updater", 1)
                    Catch ex As Exception
                        LogFile.WriteEntry(ex.Message)
                        LogFile.WriteEntry(ex.StackTrace)
                    End Try
                    Exit Sub
                End If
            Next

            LogFile.WriteEntry("Task not found, creating N3 VM Updater task for windows Task Scheduler")

            ' Create a new task definition and assign properties
            Dim td As TaskDefinition = ts.NewTask()
            td.RegistrationInfo.Description = "Automatic version control\updating for N3 VM VPN files and configuration"

            ' Create a trigger that will fire the task at this time every day with a randomization of hours
            Dim SetTime As Date = Date.Now.ToShortDateString & " " & VPNPolling.N3SchedTime
            Dim RandomSet As TimeSpan = TimeSpan.FromHours(VPNPolling.N3SchedHr)
            td.Triggers.Add(New DailyTrigger() With {.DaysInterval = VPNPolling.N3DaysInt, .StartBoundary = SetTime, .RandomDelay = RandomSet})

            ' Create an action that will launch Notepad whenever the trigger fires
            td.Actions.Add(New ExecAction("""" & PollerLoc & "\N3-VM-Updater.exe""", , PollerLoc))
            td.Principal.RunLevel = TaskRunLevel.Highest
            td.Settings.Hidden = True
            td.Settings.AllowDemandStart = True
            td.Settings.StartWhenAvailable = True
            td.Settings.DisallowStartIfOnBatteries = False

            ' Register the task in the root folder
            Try
                ts.RootFolder.RegisterTaskDefinition("N3 VM Updater", td)
            Catch
                LogFile.WriteEntry("ICNET N3 VM Updater task might already exist")
            End Try
        End Using
    End Sub

    'Create N3 Taskbar Sched Task
    Private Shared Sub N3TaskbarPinSched()
        Using ts As New TaskService()

            Dim TaskNames As TaskFolder = ts.RootFolder
            For Each t As Task In TaskNames.Tasks
                If t.Name = "N3 Taskbar Pin" Then
                    Try
                        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SchTasks", "N3 Taskbar Pin", 1)
                    Catch ex As Exception
                        LogFile.WriteEntry(ex.Message)
                        LogFile.WriteEntry(ex.StackTrace)
                    End Try
                    Exit Sub
                End If
            Next

            LogFile.WriteEntry("Task not found, creating N3 Taskbar Pin task for windows Task Scheduler")

            ' Create a new task definition and assign properties
            Dim td As TaskDefinition = ts.NewTask()
            td.RegistrationInfo.Description = "Allows creation for user Pinned taskbar icons"

            ' Create an action that will launch Notepad whenever the trigger fires
            td.Actions.Add(New ExecAction("""" & PollerLoc & "\N3VMRegisterToolbar.bat""", , PollerLoc))
            td.Principal.RunLevel = TaskRunLevel.Highest
            td.Settings.Hidden = True
            td.Settings.AllowDemandStart = True
            td.Settings.StartWhenAvailable = True
            td.Settings.DisallowStartIfOnBatteries = False

            ' Register the task in the root folder
            Try
                ts.RootFolder.RegisterTaskDefinition("N3 Taskbar Pin", td, TaskCreation.Create, "VPN User", "KeySecurity42", TaskLogonType.InteractiveToken)
            Catch
                LogFile.WriteEntry("ICNET N3 Taskbar Pin task might already exist")
            End Try
        End Using
    End Sub

End Class
