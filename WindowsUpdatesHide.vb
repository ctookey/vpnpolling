﻿
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Threading

Public Class WindowsUpdatesHide
    Inherits WindowsUpdates

    Private Protected Shared WinHideOffline As Boolean = False 'Used to determin what to rename file so processing is known if it was done offline or online
    Private Protected Shared WinHideFileExt As String = ".w10" 'Used to retry updates process offline to being done online (.w10 .prc .tmp)

    Private Protected Shared WinHideThread As New Thread(New ThreadStart(AddressOf CheckUpdatesTOHide))

    Private Protected Shared Sub HideUpdates()
        'Different file extension to allow hidding in the future for new VM with w10 extension
        If Directory.Exists(WinUpDir) Then
            If WinHideThread.ThreadState = ThreadState.Background Then
                LogFile.WriteEntry("### - Hiding known windows updates")
                VPNPolling.MessString = "Hiding known windows updates"
            Else
                Dim NewUpdates = My.Computer.FileSystem.GetFiles(WinUpDir, FileIO.SearchOption.SearchTopLevelOnly, "*.w10")
                Dim ExtUpdates = My.Computer.FileSystem.GetFiles(WinUpDir, FileIO.SearchOption.SearchTopLevelOnly, "*.tmp")
                If NewUpdates.Count > 0 Then
                    Try
                        LogFile.WriteEntry("### - Windows hide files found - W10")
                        WinHideFileExt = ".w10"
                        'Start Hiding Updates
                        WinHideThread = New Thread(New ThreadStart(AddressOf CheckUpdatesTOHide)) With {.IsBackground = True}
                        WinHideThread.Start()
                    Catch ex As Exception
                        LogFile.WriteEntry("PollingService encountered an error - Ext Updates W10'" & ex.Message & "'")
                        LogFile.WriteEntry("PollingService service Stack Trace: " & ex.StackTrace)
                        LogFile.WriteEntry("PollingService service Stack Values: " & WinHideThread.Name)
                        VPNPolling.ProblemIconAlert = True
                        VPN.Kill("Default")
                    End Try
                ElseIf ExtUpdates.Count > 0 Then
                    If Networking.CheckForInternetConnection() Then
                        Try
                            LogFile.WriteEntry("### - Internet now available checking files again")
                            WinHideFileExt = ".tmp"
                            'Start Hiding Updates
                            WinHideThread = New Thread(New ThreadStart(AddressOf CheckUpdatesTOHide))
                            WinHideThread.IsBackground = True
                            WinHideThread.Start()
                        Catch ex As Exception
                            LogFile.WriteEntry("PollingService encountered an error - Ext Updates TMP'" & ex.Message & "'")
                            LogFile.WriteEntry("PollingService service Stack Trace: " & ex.StackTrace)
                            LogFile.WriteEntry("PollingService service Stack Values: " & WinHideThread.Name)
                            VPNPolling.ProblemIconAlert = True
                            VPN.Kill("Default")
                        End Try
                    End If
                Else
                    LogFile.WriteEntry("### - Empty hiding known windows updates folder - deleting folder " & WinUpDir)
                    Directory.Delete(WinUpDir, True)
                End If
            End If
        End If
    End Sub

    Private Protected Shared Sub CheckUpdatesTOHide(Optional KB As String = "")
        Try
            If KB = "" Then
                LogFile.WriteEntry("### - Windows update - checking hidden updates . . . .")
                If Directory.Exists(WinUpDir) Then
                    Dim di As New IO.DirectoryInfo(WinUpDir)
                    Dim diar1 As IO.FileInfo() = di.GetFiles()
                    Dim dra As IO.FileInfo

                    'list the names of all files in the specified directory
                    For Each dra In diar1
                        If dra.ToString.Contains("KB") And dra.ToString.Contains(WinHideFileExt) Then
                            Dim KBTmp As String = dra.ToString.Replace(WinHideFileExt, "")
                            LogFile.WriteEntry("### - Windows update - " & KBTmp & " hidden update found")
                            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Hidden", KBTmp, Nothing) Is Nothing Or
                                My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Hidden", KBTmp, Nothing) = "Once" Then
                                KBTmp = KBTmp.Substring(2)
                                StopWinUpdate(KBTmp)
                            Else
                                'Deleting file as this has been done already
                                LogFile.WriteEntry("### - Skipping " & KBTmp & " as this update has been hidden already")
                                If File.Exists(WinUpDir & "\" & dra.ToString) Then File.Delete(WinUpDir & "\" & dra.ToString)
                            End If
                        End If
                    Next
                Else
                    LogFile.WriteEntry("### - Windows update hiding - nothing to do")
                End If
            Else
                LogFile.WriteEntry("### - Windows update - " & KB & " uninstall now needs to be hidden")
                If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Hidden", KB, Nothing) Is Nothing Or
                    My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Hidden", KB, Nothing) = "Once" Then
                    KB = KB.Substring(2)
                    StopWinUpdate(KB)
                Else
                    'Uninstalled file already hidden
                    LogFile.WriteEntry("### - Skipping " & KB & " Uninstalled update already hidden")
                End If
            End If
        Catch ex As Exception
            LogFile.WriteEntry("### - Error Win updates - " & ex.Message)
            LogFile.WriteEntry("### - Error Win updates stack - " & ex.StackTrace)
        End Try
    End Sub

    Private Protected Shared Sub StopWinUpdate(hotfixid)
        Dim KBFileName = "C:\N3-IT\WinUpdates\KB" & hotfixid & WinHideFileExt

        'Hides passed windows update
        LogFile.WriteEntry("### - Finding Windows update : " & hotfixid)
        Dim updateSession, updateSearcher
        updateSession = CreateObject("Microsoft.Update.Session")
        updateSearcher = updateSession.CreateUpdateSearcher()
        If Networking.CheckForInternetConnection() = False Then ' Checks if the internet is avilable to get updates
            LogFile.WriteEntry("### - Unable to get online to check update, running offline DB")
            WinHideOffline = True
            updateSearcher.Online = False
        Else
            WinHideOffline = False
            updateSearcher.Online = True
        End If

        Dim searchResult
        searchResult = updateSearcher.Search("IsInstalled=0 and IsHidden=0")

        Dim update, kbArticleId, index, index2
        LogFile.WriteEntry("### - " & CStr(searchResult.Updates.Count) & " found.")
        For index = 0 To searchResult.Updates.Count - 1
            update = searchResult.Updates.Item(index)
            For index2 = 0 To update.KBArticleIDs.Count - 1
                kbArticleId = update.KBArticleIDs(index2)
                LogFile.WriteEntry("### - Found : " & kbArticleId)
                If kbArticleId = hotfixid Then
                    LogFile.WriteEntry("### - Hiding update : " & update.Title)
                    update.IsHidden = True
                    'Removing file to stop running again
                    If File.Exists(KBFileName) Then
                        LogFile.WriteEntry("### - Removing KB file as nolonger needed """ & KBFileName & """")
                        File.Delete(KBFileName)
                        'Create KB name to stop processing again
                        If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Hidden", "KB" & hotfixid, Nothing) Is Nothing Then
                            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Hidden", "KB" & hotfixid, "Processed")
                        End If
                    End If
                    Exit Sub
                End If
            Next
        Next
        'Rename file as it's been processed
        If File.Exists(KBFileName) Then
            Dim KBNewName As String = "C:\N3-IT\WinUpdates\KB" & hotfixid & ".prc"
            If WinHideOffline = True Then
                KBNewName = "C:\N3-IT\WinUpdates\KB" & hotfixid & ".tmp"
            Else
                If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Hidden", "KB" & hotfixid, Nothing) Is Nothing Then
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Hidden", "KB" & hotfixid, "Once")
                ElseIf My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Hidden", "KB" & hotfixid, Nothing) = "Once" Then
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Hidden", "KB" & hotfixid, "Processed")
                End If
            End If

            LogFile.WriteEntry("### - Renaming file as it's been proccessed and not found on the system : """ & KBNewName & """")
            File.Move(KBFileName, KBNewName)
        End If
    End Sub

    Private Protected Shared Sub HideMonthlySecUp(UpdateStr As String)
        LogFile.WriteEntry("Windows update that needs hidding found")
        'Find KB Number from text line
        Dim KBRegEx As Regex = New Regex("(?<=\().*?(?=\))")
        Dim Result As String
        If KBRegEx.IsMatch(UpdateStr) Then
            Result = KBRegEx.Match(UpdateStr).Value
            Dim ExFile = WinUpDir & "\" & Result & ".txt"
            If File.Exists(ExFile) Then
                LogFile.WriteEntry("### - Already hidding")
            Else
                'removes registry key so that this get's proccessed even if it's been attempted before
                Try
                    Dim key As Microsoft.Win32.RegistryKey
                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\ICNet\CheckApp\WU\Hidden", True)
                    key.DeleteValue(Result)
                Catch
                    'do nothing
                End Try
                LogFile.WriteEntry("### - Deleteing hidden registry entry")
                File.Create(ExFile)
                LogFile.WriteEntry("### - Creating file to hide: " & ExFile)
            End If
        End If
    End Sub

    Public Shared Sub setHiddenUpdate(KB As String)
        HideMonthlySecUp(KB)
    End Sub

    Public Shared Sub SetUninstallHiddenUpdates(KB As String)
        If Not WinHideThread.IsAlive Then
            WinHideThread = New Thread(New ParameterizedThreadStart(AddressOf CheckUpdatesTOHide)) With {.IsBackground = True}
            WinHideThread.Start(KB)
        End If
    End Sub

    Public Shared Sub startHidingUpdates()
        HideUpdates()
    End Sub

    Public Shared Sub AbortThreads()
        If WinHideThread.IsAlive Then WinHideThread.Abort()
    End Sub

    Public Shared Function getThreadState()
        If WinHideThread.IsAlive Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
