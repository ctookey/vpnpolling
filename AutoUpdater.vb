﻿Imports System.IO

Public Class AutoUpdater
    'Replace autoupdater If newer version found

    Public Shared Sub ChkAutoPoller()
        ReplaceAuto()
    End Sub

    Private Shared Sub ReplaceAuto()
        Try
            LogFile.WriteEntry("Checking auto updater version")
            If File.Exists(PollerLoc & "\AutoPoller.exe") Then
                For Each prog As Process In Process.GetProcesses
                    If prog.ProcessName = "AutoPoller" Then
                        LogFile.WriteEntry("AutoPoller running unable to update, exiting")
                        Exit Sub
                    End If
                Next

                Dim AutoVerExt As Version = Version.Parse(FileVersionInfo.GetVersionInfo(PollerLoc & "\AutoPoller.exe").ProductVersion) 'Create version of file
                Dim AutoVerNew As Version = Version.Parse(FileVersionInfo.GetVersionInfo(PollerLoc & "\Upgrade\AutoPoller.exe").ProductVersion)

                'Checking versions
                If Version.op_GreaterThan(AutoVerNew, AutoVerExt) Then
                    LogFile.WriteEntry("Newer version of AutoPoller detected, Updating . . .")
                    File.Delete(PollerLoc & "\AutoPoller.exe")
                    File.Copy(PollerLoc & "\Upgrade\AutoPoller.exe", PollerLoc & "\AutoPoller.exe")
                    AutoVerExt = Version.Parse(FileVersionInfo.GetVersionInfo(PollerLoc & "\AutoPoller.exe").ProductVersion)
                    If Version.Equals(AutoVerNew, AutoVerExt) Then
                        LogFile.WriteEntry("AutoPoller updated succesfully")
                    Else
                        LogFile.WriteEntry("Failed to update Autopoller to new version " & AutoVerExt.ToString)
                    End If
                Else
                    LogFile.WriteEntry("Auto updater update not required")
                End If
            Else
                LogFile.WriteEntry("AutoPoller not detected")
                If File.Exists(PollerLoc & "\Upgrade\AutoPoller.exe") Then
                    File.Copy(PollerLoc & "\Upgrade\AutoPoller.exe", PollerLoc & "\AutoPoller.exe")
                    If File.Exists(PollerLoc & "\Upgrade\AutoPoller.exe") Then
                        LogFile.WriteEntry("AutoPoller copied succesfully")
                    End If
                End If
            End If
        Catch ex As Exception
            LogFile.WriteEntry("Unable to update Autopoller")
            LogFile.WriteEntry(ex.Message)
            LogFile.WriteEntry(ex.StackTrace)
        End Try
    End Sub
End Class
