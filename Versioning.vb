﻿Imports System.IO

Public Class Versioning

    Public Shared Sub check()
        Dim Version1 As Version = Version.Parse("2.0.0.0")
        LogFile.WriteEntry("[REG] - Checking versions")
        If Version.op_LessThan(PolCurVer, Version1) Then
            'Removes old registry structure
        Else
            LogFile.WriteEntry("[REG] - No system resets, poller version " & PolCurVer.ToString)
        End If
    End Sub

End Class
