﻿Imports System.Threading
Imports WUApiLib
Imports Newtonsoft.Json
Imports System.Windows.Threading

Public Class WindowsUpdates
    Private Shared UpdatesToDownload As New UpdateCollection
    Private Const CheckAmtSec As Integer = 3600
    Private Const CheckAmtSec24hr As Integer = 86400
    Protected Shared ReadOnly WinUpDir As String = "C:\N3-IT\WinUpdates" 'Used for hiding windows updates
    Protected Shared ReadOnly SecUpDir As String = "C:\N3-IT\SecUpdates" 'Used for installing windows security updates CABINET files
    Protected Shared ReadOnly RmvUpDir As String = "C:\N3-IT\RemoveUpdates" 'Used for uninstalling windows updates
    Protected Shared ChkUpdateAgePass As Boolean = True 'Set to when a pass is to work for windows update
    Protected Shared Timeleft As Integer = CheckAmtSec

    Private Shared WUThread As New Thread(New ThreadStart(AddressOf GetWU))
    Private Shared WUUpdateThread As New Thread(New ThreadStart(AddressOf WUTimer))

    Private Shared Function WUTimer()
        Do
            Try
                ' Wait...
                Thread.Sleep(1000)
                'Recording to keep track of when next runtime
                If Timeleft > 0 And ChkUpdateAgePass = False Then
                    Timeleft -= 1
                Else
                    Timeleft = CheckAmtSec
                    ChkUpdateAgePass = True ' Allow next check to do updates pass
                End If
            Catch
            End Try
        Loop
    End Function

    Private Shared Function GetWU()
        Try
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanStart",
                                          DateTime.Now.ToShortDateString() & " " & DateTime.Now.ToLongTimeString())

            Dim datTim1 As Date = Date.Parse(DateTime.Now.ToShortDateString())

            'Setting Windows update search to use Microsoft offline Sync file
            Dim UpdateSession = CreateObject("Microsoft.Update.Session")
            Dim UpdateSearcher As Object
            If VPNPolling.UseWSUSCAB = True Then
                Dim UpdateServiceManager = CreateObject("Microsoft.Update.ServiceManager")
                Dim UpdateService = UpdateServiceManager.AddScanPackageService("Offline Sync Service", PollerLoc & "\wsusscn2.cab", 1)
                UpdateSearcher = UpdateSession.CreateUpdateSearcher
                UpdateSearcher.ServerSelection = 3 ' ssOthers : Needed to search offline files as not an update server
                UpdateSearcher.ServiceID = UpdateService.ServiceID
            Else
                UpdateSearcher = UpdateSession.CreateUpdateSearcher

                'If the internet is avilable then serach online ot offline if not
                If Networking.CheckForInternetConnection() = True Then
                    UpdateSearcher.Online = True
                Else
                    UpdateSearcher.Online = False
                End If
            End If

            Dim intImportant = 0
            Dim intOptional = 0

            WriteResultsReg("WUScanAmt") 'Setting amount in registry
            LogFile.WriteEntry("### - Searching Windows updates .....")

            Dim result
            Try
                result = UpdateSearcher.Search(VPNPolling.WUSearchString)
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanFinish",
                                              DateTime.Now.ToShortDateString() & " " & DateTime.Now.ToLongTimeString())
                LogFile.WriteEntry("### - Finished updates scan")
            Catch ex As Exception
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanResult", 3)
                LogFile.WriteEntry("### - Windows updates search encountered an error '" & ex.Message & "'")
                Return (Nothing)
            End Try

            Dim colDownloads = result.Updates
            Dim importantNames = Nothing
            Dim optionalNames = Nothing

            If VPNPolling.UseWSUSCAB = True Then
                '@todo Not currently tested on win10 build, left to support scaning of the file if nessasry.
                For Each objentry As Object In colDownloads
                    If objentry.AutoSelectOnWebSites Then
                        LogFile.WriteEntry("### - Important - " & objentry.Title)
                        If intImportant = 0 Then
                            importantNames = objentry.Title
                        Else
                            importantNames = importantNames & "; " & objentry.Title
                        End If
                        intImportant = intImportant + 1
                        UpdatesToDownload.Add(objentry) ' Creates array of updates to install
                    Else
                        LogFile.WriteEntry("### - Optional - " & objentry.Title)
                        If intOptional = 0 Then
                            optionalNames = objentry.Title
                        Else
                            optionalNames = optionalNames & "; " & objentry.Title
                        End If
                        intOptional = intOptional + 1
                    End If
                Next
            Else
                Dim VMModel = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation\", "Model", "")
                For Each objentry In colDownloads
                    If objentry.BrowseOnly = True Then ' BrowseOnly is specifically used for whether an update is deemed optional or not (True for optional)
                        LogFile.WriteEntry("### - Optional - " & objentry.Title)
                        intOptional = intOptional + 1
                    Else
                        If objentry.Title.contains("Security Monthly Quality Rollup for Windows 7 for x64-based Systems") And Not VMModel.contains("VPN v4") Then
                            '@TODO is this required? - Sets off to hide updates of this nature as they fail to install in the VPN VM
                            LogFile.WriteEntry("### - Important to be hidden - " & objentry.Title)
                            WindowsUpdatesHide.setHiddenUpdate(objentry.Title)
                        ElseIf ChkValidUpdates(objentry) Then
                            LogFile.WriteEntry("### - Important - " & objentry.Title)
                            UpdatesToDownload.Add(objentry) ' Creates array of updates to install
                            intImportant = intImportant + 1
                        End If
                    End If
                Next
            End If

            If intImportant + intOptional > 0 Then
                If VPNPolling.PTrace = True Then LogFile.WriteEntry("### - Updates: " & intImportant & " important, " & intOptional & " optional")
                '@todo , not meant to go here? If Not intImportant = 0 Then DwnInstUpdates()
                If intImportant = 0 Then
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanResult", 1)
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUImportant", 0)
                Else
                    DwnInstUpdates() 'Download and install updates
                    VPNPolling.WSUSGetDWN = True 'Setting flag to set download of updates off
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanResult", 2)
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUImportant", intImportant)
                End If
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUOptional", intOptional)
                Return (datTim1)
            Else
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanResult", 1)
                LogFile.WriteEntry("### - No updates waiting or installing, setting scan to wait 24hrs")
                Timeleft = CheckAmtSec24hr
                ChkUpdateAgePass = False
                Return (datTim1)
            End If
        Catch ex As Exception
            LogFile.WriteEntry("### - PollingService encountered an error '" & ex.Message & "'")
            LogFile.WriteEntry("### - PollingService service stack trace: " & ex.StackTrace)
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanResult", 3)
            Return (Nothing)
        End Try
    End Function

    Private Shared Function DwnInstUpdates()
        Try
            WriteResultsReg("WUDWNAmt") 'Saving results of downloads into registry
            LogFile.WriteEntry("### - Background Windows updates started . . . .")

            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUDownloadStart",
                                              DateTime.Now.ToShortDateString() & " " & DateTime.Now.ToLongTimeString())

            Dim downloader As New UpdateDownloader
            downloader.Updates = UpdatesToDownload
            LogFile.WriteEntry("### - Background updates downloading started . . . .")
            downloader.Download()

            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUDownloadFinish",
                                 DateTime.Now.ToShortDateString() & " " & DateTime.Now.ToLongTimeString())

            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUInstallStart",
                                 DateTime.Now.ToShortDateString() & " " & DateTime.Now.ToLongTimeString())

            Dim installer As New UpdateInstaller
            installer.Updates = UpdatesToDownload
            LogFile.WriteEntry("### - Background installing updates started . . . .")
            Dim installationResult = installer.Install()

            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUInstallFinish",
                                 DateTime.Now.ToShortDateString() & " " & DateTime.Now.ToLongTimeString())

            'Output results of install
            LogFile.WriteEntry("### - Installation result: " & installationResult.ResultCode)
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUInstallResult", installationResult.ResultCode)
            If installationResult.ResultCode = 2 Then My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanResult", 1) ' If good download stop extra scans
            LogFile.WriteEntry("### - Reboot required: " & installationResult.RebootRequired)
            LogFile.WriteEntry("### - Listing of updates installed and individual installation results:")

            Dim I As Integer
            For I = 0 To UpdatesToDownload.Count - 1
                LogFile.WriteEntry("### -" & I + 1 & ": " & UpdatesToDownload.Item(I).Title & ": " & installationResult.GetUpdateResult(I).ResultCode)
            Next

            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUDownloadFinish",
                                 DateTime.Now.ToShortDateString() & " " & DateTime.Now.ToLongTimeString())

            Return (Nothing)
        Catch ex As Exception
            LogFile.WriteEntry("### - WU download Error - " & ex.Message)
            If ex.Message.Contains("Exception from HRESULT:") Then
                If ex.Message.Contains("0x80240013") Then
                    LogFile.WriteEntry("### - Error allows retry of scan, setting next pass to check for updates again.")
                    SetUpdateAgePass(True)
                Else
                    '@TODO if this required? Does it work on WIN 10
                    'Networking.MessageOut("Go to http://goo.gl/jrCk6 download and fix WU Updates")
                End If
            End If
            Return (Nothing)
        End Try
    End Function

    Public Shared Sub StartWUScan()
        Try
            If VPNPolling.PTrace = True Then LogFile.WriteEntry("### 1 - " & WUThread.IsAlive.ToString)

            'Is there a scan in progress and is a reboot required
            If GetThreadState() = False And ChkReboot() = False Then

                WUThread = New Thread(New System.Threading.ThreadStart(AddressOf GetWU)) With {.IsBackground = True}
                WUThread.Start()
                If VPNPolling.PTrace = True Then LogFile.WriteEntry("### 2 - " & WUThread.IsAlive.ToString)
            End If
        Catch ex As Exception
            LogFile.WriteEntry("Windows update threading failed " & ex.ToString)
        End Try
    End Sub

    Public Shared Function GetThreadState()
        If VPNPolling.PTrace = True Then
            Dim stackframe As New Diagnostics.StackFrame(1)
            LogFile.WriteEntry("Method calling " & System.Reflection.MethodInfo.GetCurrentMethod.Name.ToString & ": " & stackframe.GetMethod.Name.ToString)
        End If

        If VPNPolling.PTrace = True Then LogFile.WriteEntry("### 1 - " & WUThread.IsAlive.ToString)

        If Not WUThread.IsAlive And VPNPolling.OfflineWSUSEnable = True Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Shared Sub CheckUpdatesAge()
        Try
            Dim objSession = CreateObject("Microsoft.Update.Session")
            Dim objSearcher = objSession.CreateUpdateSearcher

            'Find the newest update in the list, start point and how many entries to return
            Dim colHistory = objSearcher.QueryHistory(0, 50)

            For Each objEntry As Object In colHistory
                Dim updResult As Boolean = ChkValidUpdates(objEntry)
                If objEntry.ResultCode = OperationResultCode.orcInProgress And updResult = True Then
                    LogFile.WriteEntry("### - Install not complete, checking if reboot required")
                    LogFile.WriteEntry("### - Title: " & objEntry.Title)
                    LogFile.WriteEntry("### - Update application date: " & objEntry.Date)
                    'Getting system reboot status
                    If ChkReboot() = True Then
                        LogFile.WriteEntry("### - Reboot required")
                        LogFile.WriteEntry("### - Title: " & objEntry.Title)
                        LogFile.WriteEntry("### - Update application date: " & objEntry.Date)
                        Networking.setClientDisplayMessage("Windows Update : Reboot required." & vbCrLf & "Reboot now to continue.")
                    Else
                        Networking.clrClientDisplayMessage()
                        LogFile.WriteEntry("### - Reboot not required")
                        LogFile.WriteEntry("### - Install not complete, rescanning updates")
                        VPNPolling.MessString = "Windows Updates out of date - Stopping VPNs from starting"
                        VPNPolling.ProblemIconAlert = True ' This is only set to alert the system there has been a problem
                        VPN.Kill("Default")
                        StartWUScan() 'Start a scan
                    End If
                ElseIf objEntry.ResultCode >= OperationResultCode.orcSucceeded And updResult = True Then
                    LogFile.WriteEntry("### - Title: " & objEntry.Title)
                    LogFile.WriteEntry("### - Update application date: " & objEntry.Date)

                    'Setup dates in the same format without the time element, and in UK format
                    Dim Date1str As String = objEntry.Date.Day & "/" & objEntry.Date.Month & "/" & objEntry.Date.Year
                    Dim Date2str As String = DateTime.Now.ToShortDateString

                    'Convert to Date format
                    Dim datTim1 As Date = Date.Parse(Date1str)
                    Dim datTim2 As Date = Date.Parse(Date2str)

                    LogFile.WriteEntry("### - Update date : " & datTim1)
                    LogFile.WriteEntry("### - Current date : " & datTim2)
                    LogFile.WriteEntry("### - Days different = " & DateDiff(DateInterval.Day, datTim1, datTim2).ToString & " <= " & VPNPolling.MaxDaysInstalled)

                    'Checking hytech process
                    If DateDiff(DateInterval.Day, datTim1, datTim2) > VPNPolling.MaxDaysInstalled Then
                        If ChkReboot() = False Then
                            LogFile.WriteEntry("### - Windows updates out of date - stopping VPNs from starting")
                            VPNPolling.MessString = "Windows Updates out of date - Stopping VPNs from starting"
                            VPNPolling.ProblemIconAlert = True ' This is only set to alert the system there has been a problem
                            VPN.Kill("Default")
                            StartWUScan() 'Start a scan
                        Else
                            LogFile.WriteEntry("### - Windows updates out of date - stopping VPNs from starting")
                            VPNPolling.MessString = "Windows Updates out of date - Stopping VPNs from starting"
                            VPNPolling.ProblemIconAlert = True ' This is only set to alert the system there has been a problem
                            VPN.Kill("Default")
                            LogFile.WriteEntry("### - Reboot required")
                            Networking.setClientDisplayMessage("Windows Update : Reboot required." & vbCrLf & "Reboot now to continue.")
                        End If
                    Else
                        LogFile.WriteEntry("Windows updates upto date")
                        If ChkReboot() = False Then Networking.clrClientDisplayMessage()
                        If Not My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanResult", Nothing) = 0 Then
                            'Resets the scan results as updates have been installed and stops false results
                            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanResult", 0) 'Reserved for good updates from Installed last date check
                        End If
                    End If
                    Exit For
                Else
                    If VPNPolling.PTrace = True Then
                        LogFile.WriteEntry("Windows update entry found and not counted as valid")
                        LogFile.WriteEntry("Title: " & objEntry.Title)
                        If objEntry.ClientApplicationID IsNot vbNullString Then LogFile.WriteEntry("Windows Product: " & objEntry.ClientApplicationID)
                        If objEntry.Categories.count > 0 Then LogFile.WriteEntry("Windows Category: " & objEntry.Categories.item(0).Name)
                        LogFile.WriteEntry("Update application date: " & objEntry.Date)
                        LogFile.WriteEntry("InstallCode: " & objEntry.ResultCode)
                        LogFile.WriteEntry("######################################################################################")
                    End If
                End If
            Next
        Catch ex As Exception
            If ex.Message.Contains("0x80240024") Then
                LogFile.WriteEntry("Unable to get any updates, there are none to get. Caused by not doing a scan first.")
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanResult", 4) 'Setting a scan to take place from an undeterminate state
            Else
                LogFile.WriteEntry("PollingService encountered an error '" & ex.Message & "'")
                LogFile.WriteEntry("PollingService service Stack Trace: " & ex.StackTrace)
                VPNPolling.ProblemIconAlert = True
                VPN.Kill("Default")
            End If
        End Try
    End Sub

    Public Shared Function ChkReboot()
        Dim objSysInfo = CreateObject("Microsoft.Update.SystemInfo")
        If objSysInfo.RebootRequired = True Then
            Return True
        Else
            Return False
        End If
    End Function

    'Checks to see if the named update is to be excluded or not
    'Makes sure it's not categorised as part of the Windows Defender
    Private Shared Function ChkValidUpdates(updateObj As Object) As Boolean
        Try
            Dim Category As String
            If updateObj.Categories.count > 0 Then
                Category = UCase(updateObj.Categories.item(0).Name)
            Else
                Category = "Unknown"
            End If

            Dim Product As String
            If updateObj.ClientApplicationID IsNot vbNullString Then
                Product = UCase(updateObj.ClientApplicationID)
            Else
                Product = "Unknown"
            End If

            Dim excludedUpdates() As String = {
                    "Defender Antivirus",
                    "Security Intelligence Update for Microsoft Defender Antivirus",
                    "Security Intelligence Update for Windows Defender Antivirus",
                    "Update for Windows Defender Antivirus antimalware platform",
                    "Windows Malicious Software Removal Tool",
                    "Feature update to Windows 10"
                }

            If Product = UCase("Windows Defender") Then
                If VPNPolling.PTrace = True Then LogFile.WriteEntry("Security Product detected: Windows Defender")
                Return False
            ElseIf Category = UCase("Microsoft Defender Antivirus") Then
                If VPNPolling.PTrace = True Then LogFile.WriteEntry("Security Category detected: Microsoft Defender Antivirus")
                Return False
            Else
                For Each updateExc In excludedUpdates
                    If UCase(updateObj.title).Contains(UCase(updateExc)) Then Return False
                Next
            End If

            If VPNPolling.PTrace = True Then LogFile.WriteEntry("Security update not detected as windows defender updates")
            Return True
        Catch ex As Exception
            LogFile.WriteEntry("Checkupdates encountered an error '" & ex.Message & "'")
            LogFile.WriteEntry("Checkupdates service Stack Trace: " & ex.StackTrace)
            Return True
        End Try
    End Function

    Public Shared Sub StartWUTimer()
        'Running Security thread for WU check updates age
        If Not WUUpdateThread.IsAlive Then
            WUUpdateThread = New Thread(New ThreadStart(AddressOf WUTimer)) With {.IsBackground = True}
            WUUpdateThread.Start()
        End If
    End Sub

    Public Shared Function GetUpdateAgePass()
        Return ChkUpdateAgePass
    End Function

    Public Shared Sub SetUpdateAgePass(setVal As Boolean)
        ChkUpdateAgePass = setVal
    End Sub

    Public Shared Function GetWUTimerTimeLeft()
        Dim ts = TimeSpan.FromSeconds(Timeleft.ToString())
        Dim tsString = ts.ToString("hh\:mm\:ss")
        Return tsString
    End Function

    Public Shared Sub AbortThreads()
        If WUThread.IsAlive Then WUThread.Abort()
        If WUUpdateThread.IsAlive Then WUUpdateThread.Abort()
    End Sub

End Class
