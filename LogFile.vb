﻿Imports System.IO
Imports System.Threading

Public Class LogFile
    Private Const format As String = "dd/MM/yyyy HH:mm:ss:fff"
    Private Shared LogStr, LogStr2 As New List(Of String)() 'Used to store logs to purge to file
    Private Shared LogStrUse As Boolean = False

    Private Shared FlushLogsThread As New Thread(New ThreadStart(AddressOf WriteLogProcess))

    Public Shared Function WriteEntry(ToWrite As String)
        Dim Events = New VPNPolling()
        Try
            If DateTime.Now.Minute = "00" And
                Not Left(My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "LogFileDelDate", Nothing), 5) = Left(DateTime.Now.ToLongTimeString(), 5) Then

                Using w As StreamWriter = File.AppendText(VPNPolling.LogFileLoc & "\Poller-" & DateTime.Now.ToShortDateString().Replace("/", "") & ".txt")
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "LogFileDelDate", DateTime.Now.ToLongTimeString())
                    Dim LineWrt = DateTime.Now.ToString(format) & " : Keeping " & VPNPolling.KeepLogDays & " Days Logs - Time reached"
                    If LogStrUse = False Then
                        LogStr.Add(LineWrt)
                    Else
                        LogStr2.Add(LineWrt)
                    End If
                    Filetidy()
                End Using

            End If
            If LogStrUse = False Then
                LogStr.Add(DateTime.Now.ToString(format) & " : " & ToWrite)
            Else
                LogStr2.Add(DateTime.Now.ToString(format) & " : " & ToWrite)
            End If
        Catch ex As Exception
            Events.EventLog1.WriteEntry("Unable to write to mem . . . " & vbCrLf & vbCrLf & ex.Message & vbCrLf & ex.StackTrace, EventLogEntryType.Error)
        End Try
    End Function

    Private Shared Sub Filetidy()
        Dim directory As New IO.DirectoryInfo(VPNPolling.LogFileLoc)

        For Each file As IO.FileInfo In directory.GetFiles
            If file.Extension.Equals(".txt") AndAlso (Now - file.CreationTime).Days > VPNPolling.KeepLogDays AndAlso Not file.FullName.Contains("Update") Then
                file.Delete()
            End If
        Next
    End Sub

    Private Shared Sub Log(logMessage As String, w As TextWriter)
        w.WriteLine(logMessage)
    End Sub

    Private Shared Sub WriteLogFlush()
        Dim Events = New VPNPolling()
        Try
            Using w As StreamWriter = File.AppendText(VPNPolling.LogFileLoc & "\Poller-" & DateTime.Now.ToShortDateString().Replace("/", "") & ".txt")
                Try
                    If LogStrUse = False Then
                        If VPNPolling.PTrace = True Then
                            WriteEntry("<log> - number of log entries to flush buffer 1 - " & LogStr.Count.ToString)
                        End If
                        LogStrUse = True
                        For Each Line As String In LogStr
                            Log(Line, w)
                        Next
                        LogStr.Clear()
                    Else
                        If VPNPolling.PTrace = True Then
                            WriteEntry("<log> - number of log entries to flush buffer 2 - " & LogStr2.Count.ToString)
                        End If
                        LogStrUse = False
                        For Each Line As String In LogStr2
                            Log(Line, w)
                        Next
                        LogStr2.Clear()
                    End If
                Catch ex As Exception
                    Events.EventLog1.WriteEntry("Unable to write to log . . . " & vbCrLf & vbCrLf & ex.Message & ex.StackTrace, EventLogEntryType.Error)
                End Try
            End Using
        Catch ex As Exception
            Events.EventLog1.WriteEntry("Unable to create log file " & vbCrLf & vbCrLf & ex.Message & ex.StackTrace, EventLogEntryType.Error)
        End Try
    End Sub

    Private Shared Function WriteLogProcess()
        Do
            Try
                ' Wait...
                System.Threading.Thread.Sleep(100)
                If (LogStrUse = False And LogStr.Count >= 1) Or (LogStrUse = True And LogStr2.Count >= 1) Then WriteLogFlush()
            Catch
            End Try
        Loop
    End Function

    Public Shared Sub writeLastLogs()
        WriteEntry("Last Log entry before closing ..... Shutting down")
        WriteLogFlush()
    End Sub

    Public Shared Sub startLogFlushing()
        If Not FlushLogsThread.IsAlive Then
            FlushLogsThread.IsBackground = True
            FlushLogsThread.Start()
        End If
    End Sub

    Public Shared Sub stopLogFlushing()
        If FlushLogsThread.IsAlive Then FlushLogsThread.Abort()
    End Sub

End Class
