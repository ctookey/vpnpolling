﻿Imports System.Deployment
Imports System.IO
Imports Microsoft.Win32

Module ReadReg

    Public PollerLoc As String
    Public PolCurVer As Version = Version.Parse("0.0.0.0")
    Public Sub RdReg()
        'RDREG - Read in program registry entries for configuration, if they don't exist then create them

        Try
            'Get Poller location from registry
            PollerLoc = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\System\CurrentControlSet\services\PollingService", "ImagePath", Nothing)
            PollerLoc = PollerLoc.Replace("""", "")
            PolCurVer = Version.Parse(FileVersionInfo.GetVersionInfo(PollerLoc).ProductVersion)
            PollerLoc = PollerLoc.Replace("\pollingservice.exe", "")
            If Not Directory.Exists(PollerLoc & "\Poller-Logs") Then Directory.CreateDirectory(PollerLoc & "\Poller-Logs")
            If Directory.Exists(PollerLoc & "\Poller-Logs") Then VPNPolling.LogFileLoc = PollerLoc & "\Poller-Logs"

            'Checking versions
            Versioning.check()

            LogFile.WriteEntry("[REG] - Reading registry values")

            'Create subkeys
            My.Computer.Registry.LocalMachine.CreateSubKey("SOFTWARE\ICNet")
            My.Computer.Registry.LocalMachine.CreateSubKey("SOFTWARE\ICNet\CheckApp")
            My.Computer.Registry.LocalMachine.CreateSubKey("SOFTWARE\ICNet\CheckApp\SchTasks")
            My.Computer.Registry.LocalMachine.CreateSubKey("SOFTWARE\ICNet\CheckApp\WU")
            My.Computer.Registry.LocalMachine.CreateSubKey("SOFTWARE\ICNet\CheckApp\WU\Hidden")
            My.Computer.Registry.LocalMachine.CreateSubKey("SOFTWARE\ICNet\CheckApp\WU\Security")
            My.Computer.Registry.LocalMachine.CreateSubKey("SOFTWARE\ICNet\CheckApp\AU")
            My.Computer.Registry.LocalMachine.CreateSubKey("SOFTWARE\ICNet\CheckApp\SMTP")
            My.Computer.Registry.LocalMachine.CreateSubKey("SOFTWARE\ICNet\CheckApp\Results")
            My.Computer.Registry.LocalMachine.CreateSubKey("System\CurrentControlSet\Control\Network\LocalNet")

            'Scan checking registry key (Set by client to stop checking)
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Network\LocalNet", "NetWorkingOn", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Network\LocalNet", "NetWorkingOn", 0)
                'RegPermission()
            End If

            'How many days to check VS Scan updates
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "ScanDays", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "ScanDays", 8)
                VPNPolling.ScanDays = 8
            Else
                VPNPolling.ScanDays = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "ScanDays", 8)
            End If

            'How many days to check VS Database updates
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "DBDays", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "DBDays", 3)
                VPNPolling.DBDays = 3
            Else
                VPNPolling.DBDays = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "DBDays", 3)
            End If

            'How frequently to check
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "WaitTime", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "WaitTime", 30000)
                VPNPolling.WaitTime = 30000
            Else
                VPNPolling.WaitTime = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "WaitTime", 30000)
            End If

            'Process to stop
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "ProcName", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "ProcName", "TrGUI")
                VPNPolling.ProcName = "TrGUI"
            Else
                VPNPolling.ProcName = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "ProcName", "TrGUI")
            End If

            'Turn on\off RDP and VSphere termination
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "KillRDPandVSphere", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "KillRDPandVSphere", 1)
                VPNPolling.KillRDPVSphere = True
            Else
                If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "KillRDPandVSphere", 1) = 1 Then
                    VPNPolling.KillRDPVSphere = True
                Else
                    VPNPolling.KillRDPVSphere = False
                End If
            End If

            'Turn on\off SSL VPN termination
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "KillSSLVPN", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "KillSSLVPN", 1)
                VPNPolling.KillSSLVPN = True
            Else
                If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "KillSSLVPN", 1) = 1 Then
                    VPNPolling.KillSSLVPN = True
                Else
                    VPNPolling.KillSSLVPN = False
                End If
            End If

            'Turn on\off Browser termination
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "KillBrowser", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "KillBrowser", 0)
                VPNPolling.KillBrowsers = False
            Else
                If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "KillBrowser", 0) = 1 Then
                    VPNPolling.KillBrowsers = True
                Else
                    VPNPolling.KillBrowsers = False
                End If
            End If

            'Turn on\off Log Tracing
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "Enable_Log_Tracing", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "Enable_Log_Tracing", 0)
                VPNPolling.PTrace = False
            Else
                If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "Enable_Log_Tracing", 0) = 1 Then
                    VPNPolling.PTrace = True
                Else
                    VPNPolling.PTrace = False
                End If
            End If

            'Virus Scanner being used
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "VS_Used", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "VS_Used", "Defender")
                VPNPolling.VSUsed = "McAfee"
            Else
                VPNPolling.VSUsed = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "VS_Used", "Defender")
                If Not VPNPolling.VSUsed = "Defender" Then
                    LogFile.WriteEntry("Invalid Virus Scanner must be 'Windows 10 Defender'")
                End If
            End If

            'Setting amount of sync attempts
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "SyncAttempts", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "SyncAttempts", 0)
                VPNPolling.SyncAmt = 0
            Else
                VPNPolling.SyncAmt = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "SyncAttempts", 0)
            End If

            '################################################ Windows Updates Settings ################################

            'How many days to check windows updates

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "MaxDaysInstalled", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "MaxDaysInstalled", 31)
                VPNPolling.MaxDaysInstalled = 31
            Else
                VPNPolling.MaxDaysInstalled = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "MaxDaysInstalled", 31)
                If VPNPolling.MaxDaysInstalled = 0 Then VPNPolling.MaxDaysInstalled = -1
            End If

            'Turn on Windowsupdates offline scanning (WSUS OFfline Scanning)
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "OfflineWSUSEnable", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "OfflineWSUSEnable", 1)
                VPNPolling.OfflineWSUSEnable = True
            Else
                If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "OfflineWSUSEnable", 1) = 1 Then
                    VPNPolling.OfflineWSUSEnable = True
                Else
                    VPNPolling.OfflineWSUSEnable = False
                End If
            End If

            'Turn on Windowsupdates offline scanning (WSUS OFfline Scanning)
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "UseWSUSCAB", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "UseWSUSCAB", 0)
                VPNPolling.UseWSUSCAB = False
            Else
                If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "UseWSUSCAB", 0) = 1 Then
                    VPNPolling.UseWSUSCAB = True
                Else
                    VPNPolling.UseWSUSCAB = False
                End If
            End If

            'How many days to check scan completed for WSUS offline updates (only used if WSUSoffline enabled)
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WSUSMaxDaysScan", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WSUSMaxDaysScan", 3)
                VPNPolling.WSUSMaxDaysScan = 3
            Else
                VPNPolling.WSUSMaxDaysScan = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WSUSMaxDaysScan", 3)
            End If

            'String for Windows update search (only used if WSUSoffline enabled)
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUSearchString", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUSearchString",
                    "IsInstalled=0 and Type='Software' and IsHidden=0")
                VPNPolling.WUSearchString = "IsInstalled=0 and Type='Software' and IsHidden=0"
            Else
                VPNPolling.WUSearchString = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUSearchString",
                    "IsInstalled=0 and Type='Software' and IsHidden=0")
            End If

            'Updates Warning timeouts (WSUS OFfline Scanning)
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "DwnUpdatesMins", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "DwnUpdatesMins", 30)
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "DwnUpdatesWrnMins", 5)
                VPNPolling.DwnUpdatesMins = 30
                VPNPolling.DwnUpdatesWrnMins = 5
            Else
                VPNPolling.DwnUpdatesMins = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "DwnUpdatesMins", 30)
                VPNPolling.DwnUpdatesWrnMins = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "DwnUpdatesWrnMins", 5)
            End If

            'Microsoft settings replication - AU Options
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "AUOptions", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "AUOptions", 4)
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "AUOptions", 4)
            Else
                Dim AUOption As Integer = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "AUOptions", 4)
                If AUOption >= 1 And AUOption <= 4 Then
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "AUOptions", AUOption)
                Else
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "AUOptions", 4)
                End If
            End If

            '--------------------------------------------------MICROSOFT Windows Update settings

            'Microsoft settings replication - Turn on auto updates
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "NoAutoUpdate", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "NoAutoUpdate", 0)
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "NoAutoUpdate", 0)
            Else
                Dim NoAutoUpdate As Integer = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "NoAutoUpdate", 0)
                If NoAutoUpdate = 0 Or NoAutoUpdate = 1 Then
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "NoAutoUpdate", NoAutoUpdate)
                Else
                    LogFile.WriteEntry("[REG] - Invalid entry for NoAutoUpdate using default value")
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "NoAutoUpdate", 0)
                End If
            End If

            'Microsoft settings replication - AU Options
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "AUOptions", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "AUOptions", 4)
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "AUOptions", 4)
            Else
                Dim AUOption As Integer = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "AUOptions", 4)
                If AUOption >= 1 And AUOption <= 4 Then
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "AUOptions", AUOption)
                Else
                    LogFile.WriteEntry("[REG] - Invalid entry for AUOptions using default value")
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "AUOptions", 4)
                End If
            End If

            'Microsoft settings replication - No Auto reboot with logged on users
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "NoAutoRebootWithLoggedOnUsers", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "NoAutoRebootWithLoggedOnUsers", 1)
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "NoAutoRebootWithLoggedOnUsers", 1)
            Else
                Dim NoReboot As Integer = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "NoAutoRebootWithLoggedOnUsers", 1)
                If NoReboot = 0 Or NoReboot = 1 Then
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "NoAutoRebootWithLoggedOnUsers", NoReboot)
                Else
                    LogFile.WriteEntry("[REG] - Invalid entry for NoAutoRebootWithLoggedOnUsers using default value")
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "NoAutoRebootWithLoggedOnUsers", 1)
                End If
            End If



            '################################################ Email Settings ################################

            ReadSmtpReg()

            '################################################## Automation Settings #####################################

            'Setting if system should leave extra admin file behind
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "RemoveAdminFiles", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "RemoveAdminFiles", "True")
                VPNPolling.RemoveAdminFiles = True
            Else
                If UCase(My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "RemoveAdminFiles", "True")) = "TRUE" Then
                    VPNPolling.RemoveAdminFiles = True
                Else
                    VPNPolling.RemoveAdminFiles = False
                End If
            End If

            'Setting for day interval
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "AuDaysInt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "AuDaysInt", 1)
                VPNPolling.AuDaysInt = 1
            Else
                VPNPolling.AuDaysInt = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "AuDaysInt", 1)
            End If

            'Setting for time interval h:m:s
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "AuSchedTime", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "AuSchedTime", "08:00:00")
                VPNPolling.AuSchedTime = "08:00:00"
            Else
                VPNPolling.AuSchedTime = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "AuSchedTime", "08:00:00")
            End If

            'Setting for time interval to be delayed (Allows random settings)
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "AuSchedHr", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "AuSchedHr", 2)
                VPNPolling.AuSchedHr = 2
            Else
                VPNPolling.AuSchedHr = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "AuSchedHr", 2)
            End If

            'Setting for day interval for N3 updates
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "N3DaysInt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "N3DaysInt", 1)
                VPNPolling.N3DaysInt = 1
            Else
                VPNPolling.N3DaysInt = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "N3DaysInt", 1)
            End If

            'Setting for time interval h:m:s for N3 updates
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "N3SchedTime", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "N3SchedTime", "08:00:00")
                VPNPolling.N3SchedTime = "08:00:00"
            Else
                VPNPolling.N3SchedTime = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "N3SchedTime", "08:00:00")
            End If

            'Setting for time interval to be delayed (Allows random settings)
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "N3SchedHr", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "N3SchedHr", 2)
                VPNPolling.N3SchedHr = 2
            Else
                VPNPolling.N3SchedHr = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "N3SchedHr", 2)
            End If

            'Setting amount of update attempts
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "UpdateAttempts", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "UpdateAttempts", 0)
                VPNPolling.UpdateAmt = 0
            Else
                VPNPolling.UpdateAmt = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "UpdateAttempts", 0)
            End If

            '############################################## Results Settings ############################################

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "PollerRestartAmt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "PollerRestartAmt", 0)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "VPNRestartAmt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "VPNRestartAmt", 0)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "VPNSyncAmt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "VPNSyncAmt", 0)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "WUScanAmt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "WUScanAmt", 0)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "DBUptAmt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "DBUptAmt", 0)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "VSAmt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "VSAmt", 0)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "WUDWNAmt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "WUDWNAmt", 0)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "WUOnlineSecInt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "WUOnlineSecInt", 0)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "PollingErrSecInt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "PollingErrSecInt", 0)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "VSScanSecInt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "VSScanSecInt", 0)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "VSDBSecInt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "VSDBSecInt", 0)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "WUDWNecInt", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", "WUDWNecInt", 0)
            End If

            '############################################## Client Alerts ###############################################

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Client", "IntUpgrade", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Client", "IntUpgrade", 0)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Client", "UpgradeMess", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Client", "UpgradeMess", "")
            End If

            '############################################## Misc Settings ###############################################

            'Get Checkpoint location from registry
            VPNPolling.CheckPDir = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\CheckPoint\TRAC\5.0", "PRODDIR", Nothing)
            VPNPolling.CheckPDir = VPNPolling.CheckPDir.Replace("""", "") 'Removes quotes from string

            'How many days system log to keep
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "KeepLogDays", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "KeepLogDays", 60)
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "MaxLogDays", 90)
                VPNPolling.KeepLogDays = 60
                VPNPolling.MaxLogDays = 90
            Else
                VPNPolling.KeepLogDays = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "KeepLogDays", 60)
                VPNPolling.MaxLogDays = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "MaxLogDays", 90)
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "UseSockets", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "UseSockets", 1)
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "SocketsPort", 5001)
                Networking.setUseSockets(1)
                Networking.setSockets(5001)
            Else
                Networking.setUseSockets(My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "UseSockets", 1))
                Networking.setSockets(My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "SocketsPort", 5001))
            End If

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "N3UpdateLast", Nothing) Is Nothing Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\", "N3UpdateLast", "Not Run")
            End If

            'Writing Version to registry
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "Poll_ver", PolCurVer.ToString)

            LogFile.WriteEntry("[REG] - Finished reading registry")

            My.Computer.Registry.LocalMachine.DeleteSubKeyTree("SOFTWARE\Microsoft\MSLicensing\Store\LICENSE000", False)
            LogFile.WriteEntry("[REG] - Removing Citrix License from the registry")

            'Setting PSecec Eula
            PSexectReg()

            'Disabling Win10 logon wizards
            DisableLoginWiz()

            'Remove News and Weather from taskbar
            DisNewsWaether()

            'Setting IE comptability list if it's cleared or all sites are removed - This is not needed due to IE11 no longer needed
            'IECompListReg()

        Catch ex As Exception
            LogFile.WriteEntry("[REG] - Error reading registry")
        End Try
    End Sub

    Private Sub ReadSmtpReg()
        '################################################ Email Settings ################################

        'Mail address to send emails to, seperate addresses using ;
        If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "Mailto", Nothing) Is Nothing Then
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "Mailto", "clive_tookey@baxter.com")
            VPNPolling.Mailto = "clive_tookey@baxter.com"
        Else
            VPNPolling.Mailto = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "Mailto", "clive_tookey@baxter.com")
        End If

        'Mail address to send emails from, mailbox must allow sending
        If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "Mailfrom", Nothing) Is Nothing Then
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "Mailfrom", "no-reply@icnetplc.com")
            VPNPolling.Mailfrom = "clive_tookey@baxter.com"
        Else
            VPNPolling.Mailfrom = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "Mailfrom", "no-reply@icnetplc.com")
        End If

        'Mail SMTP delivery port
        If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "smtpport", Nothing) Is Nothing Then
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "smtpport", 22)
            VPNPolling.smtpport = 22
        Else
            VPNPolling.smtpport = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "smtpport", 22)
        End If

        'Mail SMTP delivery port
        If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "smtpssl", Nothing) Is Nothing Then
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "smtpssl", "False")
            VPNPolling.smtpssl = False
        Else
            If UCase(My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "smtpssl", "False")) = "TRUE" Then
                VPNPolling.smtpssl = True
            Else
                VPNPolling.smtpssl = False
            End If
        End If

        'Mail SMTP relay server IP or address
        If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "smtpserver", Nothing) Is Nothing Then
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "smtpserver", "mail.inbax.icnetplc.com")
            VPNPolling.SmtpServerName = "mail.inbax.icnetplc.com"
        Else
            VPNPolling.SmtpServerName = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "smtpserver", "mail.inbax.icnetplc.com")
        End If

        'Mail enable sending of attachments
        If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "Attachallowed", Nothing) Is Nothing Then
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "Attachallowed", "True")
            VPNPolling.Attachallowed = True
        Else
            If UCase(My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "Attachallowed", "True")) = "TRUE" Then
                VPNPolling.Attachallowed = True
            Else
                VPNPolling.Attachallowed = False
            End If
        End If

        'ICNet Checker Debug send mail
        If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "DebugSend", Nothing) Is Nothing Then
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "DebugSend", "True")
            VPNPolling.DebugSend = True
        Else
            If UCase(My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\SMTP", "DebugSend", "True")) = "TRUE" Then
                VPNPolling.DebugSend = True
            Else
                VPNPolling.DebugSend = False
            End If
        End If
    End Sub

    'Write results for graphs into registry
    Public Sub WriteResultsReg(RegKeyName As String)
        Dim RegVal = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", RegKeyName, 0)
        RegVal += 1
        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Results", RegKeyName, RegVal)
    End Sub

    'Create Internet Explorer Comptability List
    Public Sub IECompListReg()
        Dim i As Integer
        Dim usrkey As RegistryKey = Registry.Users
        Dim names As String() = usrkey.GetSubKeyNames()
        Dim keyname As String
        For i = 0 To UBound(names) - 1
            keyname = names(i) & "\Software\Microsoft\Internet Explorer\BrowserEmulation\ClearableListData"
            'If objkey Is Nothing Then
            'If My.Computer.Registry.GetValue("HKEY_USERS\" & keyname, "UserFilter", Nothing) Is Nothing Then
            Try
                LogFile.WriteEntry("[REG] - Creating value for HKEY_USERS\" & keyname)
                My.Computer.Registry.SetValue("HKEY_USERS\" & keyname, "UserFilter", New Byte() _
                    {&H41, &H1F, &H0, &H0, &H53, &H8, &HAD, &HBA, &H5, &H0, &H0, &H0, &HF6, &H0, &H0, &H0, &H1, &H0, &H0, &H0,
                    &H5, &H0, &H0, &H0, &HC, &H0, &H0, &H0, &H34, &H9B, &HB7, &HB0, &H49, &HA3, &HD2, &H1, &H1, &H0, &H0, &H0, &H10, &H0, &H6D, &H0, &H6F,
                    &H0, &H75, &H0, &H6E, &H0, &H74, &H0, &H73, &H0, &H69, &H0, &H6E, &H0, &H61, &H0, &H69, &H0, &H2E, &H0, &H6F, &H0, &H6E, &H0, &H2E, &H0,
                    &H63, &H0, &H61, &H0, &HC, &H0, &H0, &H0, &HF4, &H78, &HF5, &H5B, &HD5, &HA3, &HD2, &H1, &H1, &H0, &H0, &H0, &HD, &H0, &H73, &H0, &H75,
                    &H0, &H6E, &H0, &H6E, &H0, &H79, &H0, &H62, &H0, &H72, &H0, &H6F, &H0, &H6F, &H0, &H6B, &H0, &H2E, &H0, &H63, &H0, &H61, &H0, &HC, &H0,
                    &H0, &H0, &H2B, &H61, &HCF, &H6A, &H51, &HAD, &HD2, &H1, &H1, &H0, &H0, &H0, &HF, &H0, &H6C, &H0, &H69, &H0, &H66, &H0, &H65, &H0, &H73,
                    &H0, &H74, &H0, &H61, &H0, &H66, &H0, &H66, &H0, &H2E, &H0, &H63, &H0, &H6F, &H0, &H2E, &H0, &H7A, &H0, &H61, &H0, &HC, &H0, &H0, &H0,
                    &HC2, &HCB, &HF3, &HC5, &H38, &H4B, &HD4, &H1, &H1, &H0, &H0, &H0, &H11, &H0, &H73, &H0, &H69, &H0, &H6E, &H0, &H67, &H0, &H68, &H0, &H65,
                    &H0, &H61, &H0, &H6C, &H0, &H74, &H0, &H68, &H0, &H2E, &H0, &H63, &H0, &H6F, &H0, &H6D, &H0, &H2E, &H0, &H73, &H0, &H67, &H0, &HC, &H0,
                    &H0, &H0, &H63, &H43, &H35, &HE2, &H89, &H6F, &HD4, &H1, &H1, &H0, &H0, &H0, &HB, &H0, &H73, &H0, &H6A, &H0, &H6F, &H0, &H67, &H0, &H2E,
                    &H0, &H6F, &H0, &H72, &H0, &H67, &H0, &H2E, &H0, &H61, &H0, &H75, &H0})
            Catch
                LogFile.WriteEntry("[REG] - Unable to create value HKEY_USERS\" & keyname)
            End Try
            'End If
        Next
    End Sub

    'Set PSExec EULA setting
    Public Sub PSexectReg()
        Dim i As Integer
        Dim usrkey As RegistryKey = Registry.Users
        Dim names As String() = usrkey.GetSubKeyNames()
        Dim keyname As String
        For i = 0 To UBound(names) - 1
            keyname = names(i) & "\SOFTWARE\Sysinternals\PsExec"
            'If objkey Is Nothing Then
            'If My.Computer.Registry.GetValue("HKEY_USERS\" & keyname, "UserFilter", Nothing) Is Nothing Then
            Try
                LogFile.WriteEntry("[REG] - Creating PSexec value for HKEY_USERS\" & keyname)
                My.Computer.Registry.SetValue("HKEY_USERS\" & keyname, "EulaAccepted", 1)
            Catch
                LogFile.WriteEntry("[REG] - Unable to create value HKEY_USERS\" & keyname)
            End Try
            'End If
        Next
    End Sub

    Public Sub DisableLoginWiz()
        DisWelcomeExp()
        LogFile.WriteEntry("[REG] - Disabling login wizards")
        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", "EnableFirstLogonAnimation", 0)
        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\OOBE", "DisablePrivacyExperience", 1)
        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\location", "Value", "Off")
        My.Computer.Registry.CurrentConfig.DeleteSubKeyTree("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\16.0\Common\OEM", False)
        My.Computer.Registry.CurrentConfig.DeleteSubKeyTree("HKEY_LOCAL_MACHINE_SOFTWARE_WOW6432Node\Microsoft\Office\16.0\Common\OEM", False)
    End Sub

    Private Sub DisWelcomeExp()
        Dim i As Integer
        Dim usrkey As RegistryKey = Registry.Users
        Dim names As String() = usrkey.GetSubKeyNames()
        Dim keyname, keyname2 As String
        For i = 0 To UBound(names) - 1
            keyname = names(i) & "\Software\Policies\Microsoft\Windows\CloudContent"
            keyname2 = names(i) & "\SOFTWARE\Microsoft\Windows\CurrentVersion\UserProfileEngagement"
            Try
                LogFile.WriteEntry("[REG] - Creating Disable Welcome Experiance value for HKEY_USERS\" & keyname)
                My.Computer.Registry.SetValue("HKEY_USERS\" & keyname, "DisableWindowsSpotlightWindowsWelcomeExperience", 0)
                My.Computer.Registry.SetValue("HKEY_USERS\" & keyname2, "ScoobeSystemSettingEnabled", 0)
            Catch
                LogFile.WriteEntry("[REG] - Unable to create value HKEY_USERS\" & keyname)
            End Try
            'End If
        Next
    End Sub

    Private Sub DisNewsWaether()
        LogFile.WriteEntry("[REG] - Disabling News and Waether in taskbar")
        My.Computer.Registry.LocalMachine.CreateSubKey("SOFTWARE\Policies\Microsoft\Windows\Windows Feeds")
        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Windows Feeds", "EnableFeeds", 0)
    End Sub

End Module
