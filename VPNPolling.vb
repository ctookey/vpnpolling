﻿Imports System.Reflection
Imports System.Threading

Public Class VPNPolling

    Public Shared MaxDaysScan, MaxDaysInstalled, ScanDays, DBDays, WSUSMaxDaysScan, KeepLogDays, MaxLogDays As Integer ' Variables used from registry
    Public Shared WaitTime, DwnUpdatesMins, DwnUpdatesWrnMins, DBupdateTries, VSScanTries, AuDaysInt, AuSchedHr, N3DaysInt, N3SchedHr, UpdateAmt, SyncAmt As Integer ' Variables used from registry
    Public Shared ProcName, VSUsed, CheckPDir, LogFileLoc, WSUSCABLoc, WUSearchString, SocDir, AuSchedTime, N3SchedTime, WDUpdateDate As String ' Variables used from registry
    Public Shared ProblemIconAlert As Boolean = False
    Private PreviousIconAlertStatus As Boolean = False
    Public Shared ScanLogfileEnable As Boolean = False
    Public Shared KillRDPVSphere As Boolean = False 'Set to kill RDP and VSphere applications if they are running
    Public Shared KillSSLVPN As Boolean = False 'Set to kill process and connections of SSLVPNs includes Cisco\Juniper connections
    Public Shared KillBrowsers As Boolean = False 'Set to kill browsers
    Public Shared OfflineWSUSEnable As Boolean = False
    Public Shared UseWSUSCAB As Boolean = False
    Public Shared WSUSGetDWN As Boolean = False
    Public Shared MessString As String = ""
    Private DebugLoc As String = Environment.GetEnvironmentVariable("TEMP") & "\" ' Used to save files and compile logs for emailing
    Public Shared PTrace As Boolean = False 'Private Listening As Boolean = True
    Public Shared BackGrndRefresh As Boolean = True 'Updates the background screen on first scan pass to update scanning fields

    'SMTP variables
    Public Shared Mailto, Mailfrom, SmtpServerName As String
    Public Shared smtpport As Integer
    Public Shared smtpssl, Attachallowed, DebugSend As Boolean

    'Automation Variables
    Public Shared RemoveAdminFiles = False

    ' Keep track of worker thread.
    Private m_oPollingThread As New Thread(New ThreadStart(AddressOf PollProcess))

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        LogFile.WriteEntry("VPN Polling Service is initiating start process")

        'Reading in registry
        RdReg()

        'Create scheduled tasks
        SchedTasks.CreateTasks()

        WriteResultsReg("PollerRestartAmt") 'Write restart amount to registry
        LogFile.WriteEntry("PollingService is starting. Version " & PolCurVer.ToString) 'Write version to log

        Dim CheckerVerInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(PollerLoc & "\ICNetCheck.exe")
        LogFile.WriteEntry("ICNetCheck Version " & CheckerVerInfo.FileVersion)

        'Write System information to logfile
        Dim t As Type = GetType(Environment)
        Dim pi As PropertyInfo() = t.GetProperties()
        Dim i As Integer
        For i = 0 To pi.Length - 1
            If Not pi(i).Name = "StackTrace" And Not pi(i).Name = "NewLine" Then LogFile.WriteEntry("VPN-VM-INFO : " & (pi(i).Name) & " : " & pi(i).GetValue(Nothing, Nothing).ToString)
        Next i

        ' Start the thread.
        '    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanResult", 0)
        '    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU", "WUScanFinish", DateTime.Now.AddDays(-360).ToShortDateString & " " & DateTime.Now.ToLongTimeString())
        m_oPollingThread.Start()

        'Running Security thread for reg and file replacement
        ApplicationSecurity.startSecurityScan()

        'Starting Log flushing as needed
        LogFile.startLogFlushing()

        'Starting the WU timer
        WindowsUpdates.StartWUTimer()

        'Starting thread to connect to client
        Networking.startThread()

        'Starting first time run full online install of components
        WindowsUpdatesInstall.IntitialiseOnline()
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        LogFile.WriteEntry("VPN Polling Service is stopping all processes")

        'Checks if it's the Autoupdater stopping the service
        For Each prog As Process In Process.GetProcesses
            If prog.ProcessName = "AutoPoller" Then
                LogFile.WriteEntry("AutoPoller is stopping the PollingService . . .")
            End If
        Next

        'Close Socket
        If Networking.getSocketConnected() = True Then
            Networking.closeSockets()
            LogFile.WriteEntry("Closing socket connections")
        End If

        ' Stop the thread.
        m_oPollingThread.Abort()
        ApplicationSecurity.StopSecurityScan()
        Networking.abortThread()
        LogFile.stopLogFlushing()
        LogFile.writeLastLogs()
    End Sub

    Private Sub PollProcess()
        ' Loops, until killed by OnStop.
        LogFile.WriteEntry("PollingService service polling thread started.")
        Do
            ' Wait...
            Thread.Sleep(30000)

            Try
                PollingPass()
            Catch ex As Exception
                LogFile.WriteEntry("PollingService encountered an error '" &
                    ex.Message & "'")
                LogFile.WriteEntry("PollingService service Stack Trace: " &
                    ex.StackTrace)
                LogFile.WriteEntry("PollingService service Source Trace: " &
                    ex.Source)
            End Try
        Loop
    End Sub

    Private Sub PollingPass()
        Try
            Networking.ICNetCheck() 'Logs if ICNetChecker is running

            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Network\LocalNet", "NetWorkingOn", 0) = 0 Then
                ' Do Stuff Here...
                LogFile.WriteEntry("..... PollingService service polling pass executed.")

                'Starting the WU timer if its not running
                WindowsUpdates.StartWUTimer()

                'Setting False before checks
                ProblemIconAlert = False
                MessString = ""

                'Alerting user of Checkpoint Rename
                ApplicationSecurity.CheckpointExeChk()

                'Check Virusscan Database
                CheckVirusScanner.CheckVSDatabase()

                'Only start system scan if Virus definitions are upto date
                If Not MessString.Contains("Virus database out of date") And Not MessString.Contains("Unable to read VS DB") Then
                    CheckVirusScanner.CheckVSScan()
                Else
                    LogFile.WriteEntry("$$$ - Skipping Scan due to out of date virus definitions, updating DB first to continue scanning")
                End If

                'check windows updates to remove, then allow hiding of the update
                If WindowsUpdates.GetThreadState() = False And
                    WindowsUpdatesInstall.getThreadState = False And
                    WindowsUpdatesHide.getThreadState() = False Then WindowsUpdatesUninstall.StartUninstallingUpdates()

                'check windows updates to hide only if updates are not installing or attempting the latest check
                If WindowsUpdates.GetThreadState() = False And
                    WindowsUpdatesInstall.getThreadState = False And
                    WindowsUpdatesUninstall.getThreadState = False Then WindowsUpdatesHide.startHidingUpdates()

                'Check if hidding updates is running and is a seperate security install task running, if so don't check until finished
                If WindowsUpdatesHide.getThreadState() = False And
                        WindowsUpdatesInstall.getThreadState = False And
                        WindowsUpdatesUninstall.getThreadState = False And
                        WindowsUpdates.GetUpdateAgePass = True Then
                    LogFile.WriteEntry("### Checking WU age, WU time lapsed . . .")
                    WindowsUpdates.CheckUpdatesAge()
                    WindowsUpdates.SetUpdateAgePass(False)
                ElseIf WindowsUpdates.GetUpdateAgePass = True Then
                    LogFile.WriteEntry("### Checking WU - Waiting to run, other WU tasks are running . . .")
                ElseIf WindowsUpdates.GetUpdateAgePass = False Then
                    LogFile.WriteEntry("### Checking WU - Pausing for timer : checking in " & WindowsUpdates.GetWUTimerTimeLeft())
                End If

                'Check if hidding updates is running and are the updates upto date running, if so don't check until finished
                If WindowsUpdatesHide.getThreadState() = False And
                    WindowsUpdates.GetThreadState() = False And
                    WindowsUpdatesUninstall.getThreadState = False Then WindowsUpdatesInstall.startInstallingUpdates()

                'Checking autoupdate and VPN VM sync amount and stopping access if failed
                UpdateAmt = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\AU", "UpdateAttempts", 0)
                SyncAmt = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp", "SyncAttempts", 0)
                If UpdateAmt >= 4 Then
                    ProblemIconAlert = True
                    LogFile.WriteEntry("Autoupdate failed to download 4 times, stopping VPN's and getting updates")
                    VPN.Kill("Default")
                    Thread.Sleep(500)
                    Tasks.AutoUpdaterCmd()
                ElseIf SyncAmt >= 4 Then
                    ProblemIconAlert = True
                    LogFile.WriteEntry("VPN VM Sync failed to download 4 times, stopping VPN's and getting updates")
                    VPN.Kill("Default")
                    Thread.Sleep(500)
                    Tasks.VPNUpdaterCmd()
                End If

                'Write log entry when alert is fired
                If ProblemIconAlert = False Then ' knows if a problem has occured and alerts the user
                    'If MessString = "" And Not WUTimerThread.ThreadState.ToString = "Background, WaitSleepJoin" Then
                    If MessString = "" Then
                        Networking.MessageOut("PollingService No Errors")
                    Else
                        Networking.MessageOut(MessString)
                    End If

                    'Start services as there are no problems
                    If PTrace = True Then LogFile.WriteEntry("Alert icon False starting VPN's")
                    VPN.StartVPN()

                    'Write success to log
                    LogFile.WriteEntry("PollingService no errors")
                ElseIf ProblemIconAlert = True Then
                    If MessString = "" Then
                        Networking.MessageOut("PollingService Errors Detected")
                    Else
                        Networking.MessageOut(MessString)
                    End If
                    WriteResultsReg("PollingErrSecInt")
                    LogFile.WriteEntry("PollingService errors detected")
                End If

                'Update Auto updater if new one is found
                AutoUpdater.ChkAutoPoller()

                'Refresh background after fixing updates to make sure all is recent
                If BackGrndRefresh = True Or (PreviousIconAlertStatus = True And ProblemIconAlert = False) Then
                    LogFile.WriteEntry("Refreshing background")
                    Tasks.BckFreshCmd()
                    BackGrndRefresh = False
                End If

                'Record previous state
                PreviousIconAlertStatus = ProblemIconAlert

                LogFile.WriteEntry("..... PollingService service polling pass finished.")
            Else
                'Abort any threads in progress
                CheckVirusScanner.AbortThreads()
                WindowsUpdates.AbortThreads()
                WindowsUpdatesInstall.AbortThreads()
                WindowsUpdatesHide.AbortThreads()

                VPN.StartVPN()
                Networking.MessageOut("PollingService Paused")
                LogFile.WriteEntry("PollingService paused")
            End If
        Catch ex As System.Exception
            LogFile.WriteEntry("PollingService encountered an error '" & ex.Message & "'")
            LogFile.WriteEntry("PollingService service stack trace: " & ex.StackTrace)
        End Try
    End Sub

    'Generates text for BGInfo
    Public Shared Function ReportGen(AmtDays As Integer) As String
        'Corrects sent value
        If AmtDays < 0 Then
            AmtDays = 0
        Else
            AmtDays += 1
        End If

        'Turns value into text
        If AmtDays > 1 Then
            Return AmtDays.ToString & " days until"
        ElseIf AmtDays = 1 Then
            Return AmtDays.ToString & " day until"
        Else
            Return "Immediate"
        End If
    End Function

End Class
