﻿Imports System.Net
Imports System.Net.Sockets
Imports System.Security.Principal
Imports System.Text
Imports System.Threading
Imports Cassia

Public Class Networking
    Private Shared clientSocket As New TcpClient()
    Private Shared UseSockets As Boolean = False
    Private Shared SocketsPort As Integer

    Private Shared StartSocThread As New Thread(New ThreadStart(AddressOf StartSocket))

    'Starting network sockets
    Public Shared Sub StartSocket()
        Try
            Dim AtpAmt = 30
            Dim Amt = 0
            While (True)
                Try
                    Thread.Sleep(1000)
                    If clientSocket.Connected = False Then
                        clientSocket.Connect("127.0.0.1", 5001)
                        LogFile.WriteEntry("@@@ - Connected")
                        MessageOut("Initiating")
                        Exit While
                    End If
                Catch ex As Exception
                    If Not ex.Message.Contains("No connection could be made because the target machine actively refused it") And VPNPolling.PTrace = False Then
                        LogFile.WriteEntry("@@@ - StrSoc - " & ex.Message)
                    ElseIf VPNPolling.PTrace = True Then
                        LogFile.WriteEntry("@@@ - StrSoc Trace - " & ex.Message)
                        LogFile.WriteEntry("@@@ - StrSoc Stack - " & ex.StackTrace)
                    Else
                        Amt = Amt + 1 ' increment if connection is due to client not being avilable
                    End If

                    If Amt = AtpAmt Then
                        LogFile.WriteEntry("@@@ - Client disconnected, no user alerts")
                        Amt = 0
                    End If
                End Try
            End While
        Catch ex As Exception
            LogFile.WriteEntry("@@@ - Poller Socket Failed - " & ex.Message)
            LogFile.WriteEntry("@@@ - Poller Socket Failed - " & ex.StackTrace)
        End Try
    End Sub

    'Used to pass messages to ICNET Checker Client
    Public Shared Sub MessageOut(Message As String)
        If VPNPolling.PTrace = True Then
            Dim stackframe As New Diagnostics.StackFrame(1)
            LogFile.WriteEntry("Method calling " & System.Reflection.MethodInfo.GetCurrentMethod.Name.ToString & ": " & stackframe.GetMethod.Name.ToString)
        End If

        Try
            If Not Message = "" And UseSockets = True Then 'Dont send empty messages or if it's not configured for sockets
                If clientSocket.Connected = True Then
                    Try
                        If VPNPolling.PTrace = True Then LogFile.WriteEntry("Sending : " & Message)
                        Dim serverStream As NetworkStream = clientSocket.GetStream()
                        Dim outStream As Byte() = Encoding.ASCII.GetBytes(Message & "$")
                        serverStream.Write(outStream, 0, outStream.Length)
                        If VPNPolling.PTrace = True Then LogFile.WriteEntry("Size: " & outStream.Length.ToString)
                        serverStream.Flush()
                    Catch ex As Exception
                        If VPNPolling.PTrace = True Then LogFile.WriteEntry("@@@ 1 - " & ex.Message)
                    End Try
                Else
                    If VPNPolling.PTrace = True Then LogFile.WriteEntry("@@@ - Socket Thread State = " & StartSocThread.ThreadState.ToString)
                    If Not StartSocThread.IsAlive Then
                        'If Not StartSocThread.ThreadState.ToString = "Background, WaitSleepJoin" And StartSocThread.ThreadState = ThreadState.Background = False Then
                        'If WUTimerThread.ThreadState = ThreadState.Background = True Or WUTimerThread.ThreadState.ToString = "Background, WaitSleepJoin" Then
                        '    WUTimerThread.Abort()
                        '    LogFile("@@@ - TIMER ABORTED")
                        'End If
                        clientSocket.Close()
                        clientSocket = New TcpClient
                        StartSocThread = New Thread(New ThreadStart(AddressOf StartSocket))
                        StartSocThread.IsBackground = True
                        StartSocThread.Start()
                        LogFile.WriteEntry("@@@ - Starting New instance")
                    End If
                End If
            End If
        Catch ex As Exception
            LogFile.WriteEntry("@@@ - Poller Message Failed - " & ex.Message)
            LogFile.WriteEntry("@@@ - Poller Message Failed - " & ex.StackTrace)
        End Try
    End Sub


    Public Shared Function CheckForInternetConnection() As Boolean
        Try
            LogFile.WriteEntry("Trying Google webclient")
            Using client = New WebClient()
                Using stream = client.OpenRead("http://www.google.com")
                    LogFile.WriteEntry("Return TRUE Google webclient")
                    Return True
                End Using
            End Using
        Catch
            Try
                LogFile.WriteEntry("Trying Google nslookup")
                Dim host As IPHostEntry = Dns.GetHostEntry("www.google.com")
                If My.Computer.Network.Ping("www.google.com") Then
                    LogFile.WriteEntry("Return TRUE Google ping")
                    Return True
                End If
            Catch
                LogFile.WriteEntry("Return FALSE Google nslookup")
                Return False
            End Try
        End Try
        LogFile.WriteEntry("Return FALSE Google ALL")
        Return False
    End Function

    Public Shared Function ICNetCheck()
        'Searching if ICNetCHecker is running as a client
        For Each prog As Process In Process.GetProcesses
            If prog.ProcessName = "ICNetCheck" Then
                Return True
            End If
        Next
        LogFile.WriteEntry("~~~ - ICNetChecker Application not running, no system alerts")
        StartClient()
        Return False
    End Function

    Private Shared Sub StartClient()
        Try
            Dim manager As ITerminalServicesManager = New TerminalServicesManager()
            Using server As ITerminalServer = manager.GetRemoteServer("localhost")
                server.Open()
                For Each session As ITerminalServicesSession In server.GetSessions()
                    Dim account As NTAccount = session.UserAccount
                    If account IsNot Nothing Then
                        LogFile.WriteEntry("~~~ - Logged on user : " & account.ToString)
                        If account.ToString.Contains("VPN User") Then
                            LogFile.WriteEntry("~~~ - Attempting to start client")

                            Shell("schtasks /run /tn ""ICNET Start""", AppWinStyle.Hide, True)

                            ' Wait...
                            System.Threading.Thread.Sleep(3000)

                            For Each prog As Process In Process.GetProcesses
                                If prog.ProcessName = "ICNetCheck" Then
                                    Exit Sub
                                End If
                            Next

                            LogFile.WriteEntry("ICNET Checker not started, trying again")
                            Tasks.StartICNETCheckerCmd()
                        Else
                            LogFile.WriteEntry("~~~ - VPN user not logged on so application not started")
                        End If
                    End If
                Next
            End Using
        Catch ex As Exception
            LogFile.WriteEntry(ex.Message)
            LogFile.WriteEntry(ex.StackTrace)
        End Try
    End Sub

    Public Shared Sub setClientDisplayMessage(Message As String)
        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Client", "IntUpgrade", 1)
        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Client", "UpgradeMess", Message)
        LogFile.WriteEntry("Setting up message to display to user . . .")
    End Sub

    Public Shared Sub clrClientDisplayMessage()
        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Client", "IntUpgrade", 0)
        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Client", "UpgradeMess", "")
        LogFile.WriteEntry("Removing message from user . . .")
    End Sub

    'Send Email
    Public Shared Function Sendmail(Subj As String, Body As String, Optional Attachment As String = "") As Boolean
        Try
            If CheckForInternetConnection() Then ' Checks if the internet is avilable to send emails
                Dim SmtpServer As New Mail.SmtpClient(VPNPolling.SmtpServerName)
                Dim mail As New Mail.MailMessage()
                SmtpServer.UseDefaultCredentials = True
                SmtpServer.EnableSsl = VPNPolling.smtpssl
                SmtpServer.Port = VPNPolling.smtpport
                mail = New Mail.MailMessage()
                mail.From = New Mail.MailAddress(VPNPolling.Mailfrom)
                mail.To.Add(VPNPolling.Mailto)
                mail.Subject = Subj
                mail.Body = Body
                If VPNPolling.Attachallowed = True And Not Attachment = "" Then
                    Dim Attch As New Mail.Attachment(Attachment)
                    mail.Attachments.Add(Attch)
                End If
                If VPNPolling.PTrace = True Then
                    LogFile.WriteEntry("/// - " & mail.From.ToString & ":" & mail.To.ToString & ":" & mail.Subject & ":" & mail.Body)
                    LogFile.WriteEntry("/// - " & SmtpServer.Host & ":" & SmtpServer.EnableSsl & ":" & SmtpServer.Port & ":" & SmtpServer.UseDefaultCredentials)
                End If
                SmtpServer.Send(mail)
                Return True
            Else
                LogFile.WriteEntry("/// - Unable to send email no internet connection")
                Return False
            End If
        Catch ex As Exception
            LogFile.WriteEntry("/// - Unable to send email . . .")
            LogFile.WriteEntry(ex.Message)
            LogFile.WriteEntry(ex.StackTrace)
            Return False
        End Try
    End Function

    Public Shared Function getDisplayMessage() As String
        Dim Result = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\Client", "UpgradeMess", Nothing)
        Return Result
    End Function

    Public Shared Function getSocketConnected()
        Return clientSocket.Connected
    End Function

    Public Shared Sub closeSockets()
        clientSocket.Close()
    End Sub

    Public Shared Sub startThread()
        If Not StartSocThread.IsAlive Then
            StartSocThread.IsBackground = True
            StartSocThread.Start()
        End If
    End Sub

    Public Shared Sub abortThread()
        If StartSocThread.IsAlive Then StartSocThread.Abort()
    End Sub

    Public Shared Sub setSockets(setVal As Integer)
        SocketsPort = setVal
    End Sub

    Public Shared Sub setUseSockets(setVal As Boolean)
        UseSockets = setVal
    End Sub

    Public Shared Function getUseSockets()
        Return SocketsPort
    End Function

End Class
