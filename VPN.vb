﻿Imports System.IO
Imports System.Net.NetworkInformation
Imports System.ServiceProcess
Imports System.Threading

Public Class VPN

    Public Shared Sub Kill(exename As String)
        Try
            'Checking what process name to kill
            Dim KillProcName As String
            If exename = "Default" Then
                KillProcName = VPNPolling.ProcName
            Else
                'Remove DIR and exe from name to be able to kill process
                exename = exename.Replace(VPNPolling.CheckPDir, "")
                exename = exename.Replace(".exe", "")

                'Write error to log file
                VPNPolling.MessString = "Checkpoint EXE renamed (" & exename & "), Stopping Checkpoint"
                LogFile.WriteEntry("Checkpoint EXE renamed (" & exename & "), Stopping Checkpoint")

                'Kill process Name
                KillProcName = exename
            End If

            EachProc(KillProcName)

            'Stop services to processes
            Dim GLobalPRlt As Boolean = StartStop("PanGPS", "Global Protect", "STOP") 'Global Protect VPN service
            Dim CiscoRlt As Boolean = StartStop("CVPND", "Cisco", "STOP") 'Cisco VPN service
            Dim CiscoAnyRlt As Boolean = StartStop("vpnagent", "Cisco AnyConnect", "STOP") 'Cisco AnyConnect VPN service
            Dim CheckPtRlt As Boolean = StartStop("TracSrvWrapper", "Checkpoint", "STOP") 'Checkpoint VPN service
            Dim CheckPtWatch As Boolean = StartStop("EPWD", "Checkpoint Watcher", "STOP") 'Checkpoint Watcher VPN service
            Dim JuniperRlt As Boolean = StartStop("dsNcService", "Juniper", "STOP") 'Juniper VPN service
            Dim SonicWallRlt As Boolean = StartStop("NgVpnMgr", "SonicWall", "STOP") 'SonicWall VPN service
            Dim NetscalerRlt As Boolean = StartStop("nsverctl", "Citrix Netscaler", "STOP") 'SonicWall VPN service

        Catch ex As Exception
            LogFile.WriteEntry("Poller KillVPN Failed - " & ex.Message)
            LogFile.WriteEntry("Poller KillVPN Failed - " & ex.StackTrace)
        End Try
    End Sub

    Private Shared Sub EachProc(KillProcName As String)
        'Killing process
        For Each prog As Process In Process.GetProcesses
            If prog.ProcessName = KillProcName Then
                LogFile.WriteEntry(KillProcName & " Killed")
                prog.Kill()
                VPNChkDisconnect("CHKP") 'Check if VPN is connected and then disconnect gracefully if connected
            ElseIf prog.ProcessName = "mstsc" Or prog.ProcessName = "VpxClient" And VPNPolling.KillRDPVSphere = True Then
                'Kills extra processes of RDP and Vsphere client in case the VPN line is not dropped
                LogFile.WriteEntry("RDP or VM connection Killed")
                prog.Kill()
            ElseIf prog.ProcessName = "vpngui" And VPNPolling.KillSSLVPN = True Then
                'Kills extra processes of Cisco and Juniper connections in case the VPN line is not dropped
                LogFile.WriteEntry("Cisco or Juniper connection Killed")
                prog.Kill()
                VPNChkDisconnect("SSL") 'Check if VPN is connected and then disconnect gracefully if connected
            ElseIf prog.ProcessName = "ngmonitor" And VPNPolling.KillSSLVPN = True Then
                'Kills extra processes of SonicWall connections in case the VPN line is not dropped
                LogFile.WriteEntry("SonicWall connection Killed")
                'prog.Kill()
                VPNChkDisconnect("SSL") 'Check if VPN is connected and then disconnect gracefully if connected
            ElseIf prog.ProcessName = "SecureLinkCM" Or prog.ProcessName = "SelfServicePlugin" And VPNPolling.KillSSLVPN = True Then
                'Kills extra processes of Secure Link and Citrix clients in case the VPN line is not dropped
                LogFile.WriteEntry("Secure Link or Citrix connection Killed")
                prog.Kill()
            ElseIf prog.ProcessName = "PanGPA" And VPNPolling.KillSSLVPN = True Then
                'Kills extra processes of Globalprotect clients in case the VPN line is not dropped
                LogFile.WriteEntry("Globalprotect connection Killed")
                prog.Kill()
            ElseIf (prog.ProcessName = "TeamViewer" Or prog.ProcessName = "tv_w32" Or prog.ProcessName = "tv_x64") And VPNPolling.KillSSLVPN = True Then
                'Kills extra processes of TeamViewer clients in case the VPN line is not dropped
                LogFile.WriteEntry("TeamViewer connection Killed")
                prog.Kill()
            ElseIf prog.ProcessName.Contains("VNC-Viewer") And VPNPolling.KillSSLVPN = True Then
                'Kills extra processes of VNC clients in case the VPN line is not dropped
                LogFile.WriteEntry("VNC connection Killed")
                prog.Kill()
            ElseIf prog.ProcessName = "wfica32" And VPNPolling.KillSSLVPN = True Then
                'Kills extra processes of Citrix clients in case the VPN line is not dropped
                LogFile.WriteEntry("Citrix connection Killed")
                prog.Kill()
            ElseIf prog.ProcessName = "nsload" And VPNPolling.KillSSLVPN = True Then
                'Kills extra processes of Citrix netscaler (Gateway) clients in case the VPN line is not dropped
                LogFile.WriteEntry("Citrix Netscaler (Gateway) connection Killed")
                prog.Kill()
            ElseIf prog.ProcessName = "wksprt" And VPNPolling.KillSSLVPN = True Then
                'Kills extra processes of RemoteApp and Desktop clients (Windows Service) in case the VPN line is not dropped
                LogFile.WriteEntry("RemoteApp and Desktop clients connection Killed")
                prog.Kill()
            ElseIf prog.ProcessName = "iexplore" And VPNPolling.KillSSLVPN = True And VPNPolling.KillBrowsers = True Then
                'Kills extra processes of Internet Explorer in case the VPN line is not dropped
                LogFile.WriteEntry("Internet Explorer Killed")
                prog.Kill()
            ElseIf prog.ProcessName = "chrome" And VPNPolling.KillSSLVPN = True And VPNPolling.KillBrowsers = True Then
                'Kills extra processes of Chrome in case the VPN line is not dropped
                LogFile.WriteEntry("Chrome Killed")
                prog.Kill()
            ElseIf prog.ProcessName = "firefox" And VPNPolling.KillSSLVPN = True And VPNPolling.KillBrowsers = True Then
                'Kills extra processes of FireFox in case the VPN line is not dropped
                LogFile.WriteEntry("Firefox Killed")
                prog.Kill()
            End If
        Next
    End Sub

    Private Shared Sub VPNChkDisconnect(ScanOpt As String)
        Try
            Dim adapters As NetworkInterface() = NetworkInterface.GetAllNetworkInterfaces()
            Dim adapter As NetworkInterface
            For Each adapter In adapters
                If adapter.Description.Contains("Check Point Virtual Network Adapter For Endpoint VPN Client") And adapter.OperationalStatus = OperationalStatus.Up And ScanOpt = "CHKP" Then
                    Dim VPNDisc = New Thread(New System.Threading.ThreadStart(AddressOf VPNDisconnectCMD))
                    VPNDisc.IsBackground = True
                    VPNDisc.Start()
                    Exit For
                ElseIf adapter.Description.Contains("Cisco AnyConnect Secure Mobility Client") And adapter.OperationalStatus = OperationalStatus.Up And ScanOpt = "SSL" Then
                    Dim VPNDisc = New Thread(New System.Threading.ThreadStart(AddressOf CiscoANYVPNDisconnectCMD))
                    VPNDisc.IsBackground = True
                    VPNDisc.Start()
                    Exit For
                ElseIf adapter.Description.Contains("Cisco Systems VPN Adapter for 64-bit Windows") And adapter.OperationalStatus = OperationalStatus.Up And ScanOpt = "SSL" Then
                    Dim VPNDisc = New Thread(New System.Threading.ThreadStart(AddressOf CiscoVPNDisconnectCMD))
                    VPNDisc.IsBackground = True
                    VPNDisc.Start()
                    Exit For
                ElseIf adapter.Description.Contains("Juniper Network Connect Virtual Adapter") And adapter.OperationalStatus = OperationalStatus.Up And ScanOpt = "SSL" Then
                    Dim VPNDisc = New Thread(New System.Threading.ThreadStart(AddressOf JuniperVPNDisconnectCMD))
                    VPNDisc.IsBackground = True
                    VPNDisc.Start()
                    Exit For
                ElseIf adapter.Description.Contains("SonicWall VPN Connection") And adapter.OperationalStatus = OperationalStatus.Up And ScanOpt = "SSL" Then
                    Dim VPNDisc = New Thread(New System.Threading.ThreadStart(AddressOf SonicWallVPNDisconnectCMD))
                    VPNDisc.IsBackground = True
                    VPNDisc.Start()
                    Exit For
                End If
            Next adapter
        Catch ex As Exception
            LogFile.WriteEntry("Poller VPN Check Failed - " & ex.Message)
            LogFile.WriteEntry("Poller VPN Check Failed - " & ex.StackTrace)
        End Try
    End Sub

    'Stops or starts the mentioned service process
    Private Shared Function StartStop(SrvNam As String, VPNName As String, State As String)
        If VPNPolling.PTrace = True Then
            Dim stackframe As New Diagnostics.StackFrame(1)
            LogFile.WriteEntry("Method calling " & System.Reflection.MethodInfo.GetCurrentMethod.Name.ToString & ": " & stackframe.GetMethod.Name.ToString)
        End If

        Try
            'Does service exist
            Dim servicesButNotDevices As ServiceController() = ServiceController.GetServices()
            Dim ServiceExists As Boolean = False

            For Each services As ServiceController In servicesButNotDevices
                If services.ServiceName = SrvNam Then 'May also use DispalyName property depending on your use case
                    ServiceExists = True
                    Exit For
                End If
            Next

            'Bug as this stops and starts
            If ServiceExists = True Then
                Dim TimeoutVal As Integer = 40
                Dim Timeout As Integer = 0
                Dim service As ServiceController = New ServiceController(SrvNam)
                'Start the service if it's stopped
                If (service.Status.Equals(ServiceControllerStatus.Stopped) Or
                        service.Status.Equals(ServiceControllerStatus.StopPending)) And State = "START" Then
                    Try
                        LogFile.WriteEntry("Starting " & VPNName & " service")
                        service.Start()
                    Catch ex As Exception
                        LogFile.WriteEntry(ex.Message)
                    End Try
                    Thread.Sleep(500)
                    If service.Status.Equals(ServiceControllerStatus.Running) Then
                        Return True
                    Else
                        Return False
                    End If
                ElseIf (service.Status.Equals(ServiceControllerStatus.Stopped) Or
                       service.Status.Equals(ServiceControllerStatus.StopPending)) And State = "STOP" Then
                    LogFile.WriteEntry(VPNName & " service already stopped")
                    Return True
                ElseIf service.Status.Equals(ServiceControllerStatus.Running) And State = "START" Then
                    LogFile.WriteEntry(VPNName & " service already started")
                    Return True
                ElseIf service.Status.Equals(ServiceControllerStatus.Running) And State = "STOP" Then
                    'Stop the service if it's running
                    LogFile.WriteEntry("Stopping - waiting for " & VPNName & " to stop")
                    Try
                        service.Stop()
                    Catch ex As Exception
                        LogFile.WriteEntry(ex.Message)
                    End Try
                    Thread.Sleep(500)
                    If service.Status.Equals(ServiceControllerStatus.Stopped) Or
                        service.Status.Equals(ServiceControllerStatus.StopPending) Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
                Return False
            Else
                LogFile.WriteEntry("Service '" & SrvNam & "' is not installed - Skipping.")
                Return True
            End If
        Catch Ex As Exception
            LogFile.WriteEntry(Ex.Message)
            LogFile.WriteEntry(Ex.StackTrace)
            Return False
        End Try
    End Function

    Private Shared Sub VPNDisconnectCMD()
        Try
            LogFile.WriteEntry("Disconnecting Checkpoint VPN . . . .")
            Shell("C:\Program Files (x86)\CheckPoint\Endpoint Connect\trac disconnect", AppWinStyle.Hide, True, 60)
            LogFile.WriteEntry("Disconnected Checkpoint VPN")
        Catch ex As Exception
            LogFile.WriteEntry("Poller VPN CMD Failed - " & ex.Message)
            LogFile.WriteEntry("Poller VPN CMD Failed - " & ex.StackTrace)
        End Try
    End Sub

    'Cisco VPN Disconnect
    Private Shared Sub CiscoVPNDisconnectCMD()
        Try
            LogFile.WriteEntry("Disconnecting Cisco VPN . . . .")
            Shell("C:\Program Files (x86)\Cisco Systems\VPN Client\vpnclient.exe disconnect", AppWinStyle.Hide, True, 60)
            LogFile.WriteEntry("Disconnected Cisco VPN")
        Catch ex As Exception
            LogFile.WriteEntry("Poller Cisco VPN CMD Failed - " & ex.Message)
            LogFile.WriteEntry("Poller Cisco VPN CMD Failed - " & ex.StackTrace)
        End Try
    End Sub

    'Cisco AnyConnectVPN Disconnect
    Private Shared Sub CiscoANYVPNDisconnectCMD()
        Try
            LogFile.WriteEntry("Disconnecting Cisco AnyConnect VPN . . . .")
            Shell("C:\Program Files (x86)\Cisco\Cisco AnyConnect Secure Mobility Client\vpnagent.exe stop", AppWinStyle.Hide, True, 60)
            LogFile.WriteEntry("Disconnected Cisco AnyConnect VPN")
        Catch ex As Exception
            LogFile.WriteEntry("Poller Cisco AnyConnect VPN CMD Failed - " & ex.Message)
            LogFile.WriteEntry("Poller Cisco AnyConnect VPN CMD Failed - " & ex.StackTrace)
        End Try
    End Sub

    'Juniper VPN Disconnect
    Private Shared Sub JuniperVPNDisconnectCMD()
        Try
            LogFile.WriteEntry("Disconnecting Juniper VPN . . . .")
            Shell("C:\Program Files (x86)\Juniper Networks\Network Connect 8.1\nclauncher.exe -stop", AppWinStyle.Hide, True, 60)
            LogFile.WriteEntry("Disconnected Juniper VPN")
        Catch ex As Exception
            LogFile.WriteEntry("Poller Juniper VPN CMD Failed - " & ex.Message)
            LogFile.WriteEntry("Poller Juniper VPN CMD Failed - " & ex.StackTrace)
        End Try
    End Sub

    'SonicWall VPN Disconnect
    Private Shared Sub SonicWallVPNDisconnectCMD()
        Try
            If File.Exists("C:\Windows\system32\ngdial.exe") Then
                LogFile.WriteEntry("Disconnecting SonicWall VPN . . . .")
                Shell("C:\Windows\system32\ngdial.exe ""SonicWall VPN Connection"" -disconnect", AppWinStyle.Hide, True, 60)
                LogFile.WriteEntry("Disconnected SonicWall VPN")
            Else
                LogFile.WriteEntry("SonicWall VPN not installed")
            End If
        Catch ex As Exception
            LogFile.WriteEntry("Poller SonicWall VPN CMD Failed - " & ex.Message)
            LogFile.WriteEntry("Poller SonicWall VPN CMD Failed - " & ex.StackTrace)
        End Try
    End Sub

    Public Shared Sub StartVPN()
        If VPNPolling.PTrace = True Then
            Dim stackframe As New Diagnostics.StackFrame(1)
            LogFile.WriteEntry("Method calling " & System.Reflection.MethodInfo.GetCurrentMethod.Name.ToString & ": " & stackframe.GetMethod.Name.ToString)
        End If

        'Start services as there are no problems
        Dim GLobalPRlt As Boolean = StartStop("PanGPS", "Global Protect", "START") 'Global Protect VPN service
        Dim CiscoRlt As Boolean = StartStop("CVPND", "Cisco", "START") 'Cisco VPN service
        Dim CiscoAnyRlt As Boolean = StartStop("vpnagent", "Cisco AnyConnect", "START") 'Cisco AnyConnect VPN service
        Dim CheckPtRlt As Boolean = StartStop("TracSrvWrapper", "Checkpoint", "START") 'Checkpoint VPN service
        Dim CheckPtWatch As Boolean = StartStop("EPWD", "Checkpoint Watcher", "START") 'Checkpoint Watcher VPN service
        Dim JuniperRlt As Boolean = StartStop("dsNcService", "Juniper", "START") 'Juniper VPN service
        Dim SonicWallRlt As Boolean = StartStop("NgVpnMgr", "SonicWall", "START") 'SonicWall VPN service
        Dim NetscalerRlt As Boolean = StartStop("nsverctl", "Citrix Netscaler", "START") 'SonicWall VPN service
    End Sub
End Class
