﻿Imports System.IO
Imports System.Management.Automation
Imports System.Security.AccessControl
Imports System.Security.Principal
Imports System.Threading
Imports Microsoft.Win32

Public Class ApplicationSecurity
    'Get machine name and username for N3 and administrator
    Private Shared ReadOnly N3User As String = Environment.GetEnvironmentVariable("COMPUTERNAME") & "\VPN User"
    Private Shared ReadOnly LAdm As String = Environment.GetEnvironmentVariable("COMPUTERNAME") & "\VPN Support"
    Private Shared ReadOnly Admin As String = Environment.GetEnvironmentVariable("COMPUTERNAME") & "\Administrator"

    Private Shared PollingSecThread As New Thread(New ThreadStart(AddressOf SetPollerSecurity))

    Public Shared Sub SetPauseRegPermission()
        Try
            Dim command As New PSCommand()
            command.AddScript("$acl= get-acl -path ""hklm:\system\currentcontrolset\control\Network\LocalNet""")
            command.AddScript("$rule=new-object system.security.accesscontrol.registryaccessrule ""BUILTIN\Users"",""FullControl"", ""Allow""")
            command.AddScript("$acl.addaccessrule($rule)")
            command.AddScript("$acl|set-acl")
            Dim powershell As Management.Automation.PowerShell = PowerShell.Create()
            powershell.Commands = command
            powershell.Invoke()
        Catch ex As Exception
            LogFile.WriteEntry("Error Reg Permissions" & vbCrLf & ex.Message)
            VPN.Kill("Default")
        End Try
    End Sub


    Public Shared Sub SetPollerSecurity()
        Do
            ' Wait...
            System.Threading.Thread.Sleep(2000)
            Try
                'Set Registry ICNET Checker security settings
                ICNETRegPermissionResults()
                ICNETRegPermissionSMTP()
                ICNETRegPermissionWU()
                ICNETRegPermissionWUHidden()
                ICNETRegPermissionWUSecurity()
                ICNETRegPermissionAU()
                ICNETRegPermissionSchTasks()
                ICNETRegPermissionClient()
                ICNETRegPermission()
                'SetAutoUpdates() 'Changes Windows update settings to default settings

                'Set Poller executable security settings
                PollerExecutableSec()
            Catch ex As Exception
                LogFile.WriteEntry("*** - Poller Security Error - ***")
                LogFile.WriteEntry("PollingService encountered an error '" &
                    ex.Message & "'")
                LogFile.WriteEntry("PollingService service Stack Trace: " &
                    ex.StackTrace)
                LogFile.WriteEntry("PollingService service Source Trace: " &
                    ex.Source)
            End Try
        Loop
    End Sub

    Private Shared Sub ICNETRegPermission()
        Dim rs As New RegistrySecurity()
        Dim rk, rl As RegistryKey

        ' Attempt to change permissions for the key.
        Try
            rl = Registry.LocalMachine
            rk = rl.OpenSubKey("SOFTWARE\ICNet\CheckApp", True)
            rs.SetAccessRuleProtection(True, True)
            rs.SetAccessRule(New RegistryAccessRule("NT AUTHORITY\SYSTEM",
               RegistryRights.FullControl Or RegistryRights.TakeOwnership,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(LAdm,
               RegistryRights.FullControl,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
               RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rk.SetAccessControl(rs)

        Catch ex As Exception
            LogFile.WriteEntry("Error Setting Poller Permissions ICNETCHK" & vbCrLf & ex.Message)
            VPN.Kill("Default")
        End Try
    End Sub

    Private Shared Sub ICNETRegPermissionWU()
        Dim rs As New RegistrySecurity()
        Dim rl, rk2 As RegistryKey

        ' Attempt to change permissions for the key.
        Try
            'Setrules for rk2 WU subkey
            rl = Registry.LocalMachine
            rk2 = rl.OpenSubKey("SOFTWARE\ICNet\CheckApp\WU", True)
            rs.SetAccessRuleProtection(True, True)
            rs.SetAccessRule(New RegistryAccessRule("NT AUTHORITY\SYSTEM",
               RegistryRights.FullControl Or RegistryRights.TakeOwnership,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(LAdm,
               RegistryRights.FullControl,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
               RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rk2.SetAccessControl(rs)

        Catch ex As Exception
            LogFile.WriteEntry("Error Setting Poller Permissions WU " & vbCrLf & ex.Message)
            VPN.Kill("Default")
        End Try
    End Sub

    Private Shared Sub ICNETRegPermissionWUHidden()
        Dim rs As New RegistrySecurity()
        Dim rl, rk2 As RegistryKey

        ' Attempt to change permissions for the key.
        Try
            'Setrules for rk2 WU subkey
            rl = Registry.LocalMachine
            rk2 = rl.OpenSubKey("SOFTWARE\ICNet\CheckApp\WU\Hidden", True)
            rs.SetAccessRuleProtection(True, True)
            rs.SetAccessRule(New RegistryAccessRule("NT AUTHORITY\SYSTEM",
               RegistryRights.FullControl Or RegistryRights.TakeOwnership,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(LAdm,
               RegistryRights.FullControl,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
               RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rk2.SetAccessControl(rs)

        Catch ex As Exception
            LogFile.WriteEntry("Error Setting Poller Permissions WU Hidden " & vbCrLf & ex.Message)
            VPN.Kill("Default")
        End Try
    End Sub

    Private Shared Sub ICNETRegPermissionWUSecurity()
        Dim rs As New RegistrySecurity()
        Dim rl, rk2 As RegistryKey

        ' Attempt to change permissions for the key.
        Try
            'Setrules for rk2 WU subkey
            rl = Registry.LocalMachine
            rk2 = rl.OpenSubKey("SOFTWARE\ICNet\CheckApp\WU\Security", True)
            rs.SetAccessRuleProtection(True, True)
            rs.SetAccessRule(New RegistryAccessRule("NT AUTHORITY\SYSTEM",
               RegistryRights.FullControl Or RegistryRights.TakeOwnership,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(LAdm,
               RegistryRights.FullControl,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
               RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rk2.SetAccessControl(rs)

        Catch ex As Exception
            LogFile.WriteEntry("Error Setting Poller Permissions WU Security " & vbCrLf & ex.Message)
            VPN.Kill("Default")
        End Try
    End Sub

    Private Shared Sub ICNETRegPermissionAU()
        Dim rs As New RegistrySecurity()
        Dim rl, rk2 As RegistryKey

        ' Attempt to change permissions for the key.
        Try
            'Setrules for rk2 AU subkey
            rl = Registry.LocalMachine
            rk2 = rl.OpenSubKey("SOFTWARE\ICNet\CheckApp\AU", True)
            rs.SetAccessRuleProtection(True, True)
            rs.SetAccessRule(New RegistryAccessRule("NT AUTHORITY\SYSTEM",
               RegistryRights.FullControl Or RegistryRights.TakeOwnership,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(LAdm,
               RegistryRights.FullControl,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
               RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rk2.SetAccessControl(rs)

        Catch ex As Exception
            LogFile.WriteEntry("Error Setting Poller Permissions AU " & vbCrLf & ex.Message)
            VPN.Kill("Default")
        End Try
    End Sub

    Private Shared Sub ICNETRegPermissionSchTasks()
        Dim rs As New RegistrySecurity()
        Dim rl, rk2 As RegistryKey

        ' Attempt to change permissions for the key.
        Try
            'Setrules for rk2 AU subkey
            rl = Registry.LocalMachine
            rk2 = rl.OpenSubKey("SOFTWARE\ICNet\CheckApp\SchTasks", True)
            rs.SetAccessRuleProtection(True, True)
            rs.SetAccessRule(New RegistryAccessRule("NT AUTHORITY\SYSTEM",
               RegistryRights.FullControl Or RegistryRights.TakeOwnership,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(LAdm,
               RegistryRights.FullControl,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
               RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rk2.SetAccessControl(rs)

        Catch ex As Exception
            LogFile.WriteEntry("Error Setting Poller Permissions SchTasks " & vbCrLf & ex.Message)
            VPN.Kill("Default")
        End Try
    End Sub

    Private Shared Sub ICNETRegPermissionClient()
        Dim rs As New RegistrySecurity()
        Dim rl, rk2 As RegistryKey

        ' Attempt to change permissions for the key.
        Try
            'Setrules for rk2 AU subkey
            rl = Registry.LocalMachine
            rk2 = rl.OpenSubKey("SOFTWARE\ICNet\CheckApp\Client", True)
            rs.SetAccessRuleProtection(True, True)
            rs.SetAccessRule(New RegistryAccessRule("NT AUTHORITY\SYSTEM",
               RegistryRights.FullControl Or RegistryRights.TakeOwnership,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(LAdm,
               RegistryRights.FullControl,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
               RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rk2.SetAccessControl(rs)

        Catch ex As Exception
            LogFile.WriteEntry("Error Setting Poller Permissions Client " & vbCrLf & ex.Message)
            VPN.Kill("Default")
        End Try
    End Sub

    Private Shared Sub ICNETRegPermissionSMTP()
        Dim rs As New RegistrySecurity()
        Dim rl, rk2 As RegistryKey

        ' Attempt to change permissions for the key.
        Try
            'Setrules for rk2 SMTP subkey
            rl = Registry.LocalMachine
            rk2 = rl.OpenSubKey("SOFTWARE\ICNet\CheckApp\SMTP", True)
            rs.SetAccessRuleProtection(True, True)
            rs.SetAccessRule(New RegistryAccessRule("NT AUTHORITY\SYSTEM",
               RegistryRights.FullControl Or RegistryRights.TakeOwnership,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(LAdm,
               RegistryRights.FullControl,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
               RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rk2.SetAccessControl(rs)

        Catch ex As Exception
            LogFile.WriteEntry("Error Setting Poller Permissions SMTP " & vbCrLf & ex.Message)
            VPN.Kill("Default")
        End Try
    End Sub

    Private Shared Sub ICNETRegPermissionResults()
        Dim rs As New RegistrySecurity()
        Dim rl, rk2 As RegistryKey

        ' Attempt to change permissions for the key.
        Try
            'Setrules for rk2 SMTP subkey
            rl = Registry.LocalMachine
            rk2 = rl.OpenSubKey("SOFTWARE\ICNet\CheckApp\Results", True)
            rs.SetAccessRuleProtection(True, True)
            rs.SetAccessRule(New RegistryAccessRule("NT AUTHORITY\SYSTEM",
               RegistryRights.FullControl Or RegistryRights.TakeOwnership,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(LAdm,
               RegistryRights.FullControl,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.SetAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(Admin,
                RegistryRights.ChangePermissions Or RegistryRights.SetValue Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
               RegistryRights.QueryValues Or RegistryRights.EnumerateSubKeys Or RegistryRights.Notify Or RegistryRights.ReadKey Or RegistryRights.SetValue Or RegistryRights.CreateSubKey,
               InheritanceFlags.None,
               PropagationFlags.None,
               AccessControlType.Allow))
            rs.AddAccessRule(New RegistryAccessRule(N3User,
                RegistryRights.ChangePermissions Or RegistryRights.Delete,
                InheritanceFlags.None,
                PropagationFlags.None,
                AccessControlType.Deny))
            rk2.SetAccessControl(rs)

        Catch ex As Exception
            LogFile.WriteEntry("Error Setting Poller Permissions Results " & vbCrLf & ex.Message)
            VPN.Kill("Default")
        End Try
    End Sub

    Private Shared Sub PollerExecutableSec()
        Try
            'Get Poller location from registry
            Dim Filename As String = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\System\CurrentControlSet\services\PollingService", "ImagePath", Nothing)
            Filename = Filename.Replace("""", "")

            'Gets the current File Security
            Dim PolSecFileACL As FileSecurity = File.GetAccessControl(Filename)

            'Make System file owner
            Dim SYS_USR As NTAccount = New NTAccount("SYSTEM")
            PolSecFileACL.SetOwner(SYS_USR)

            'Remove inheritance but copy the values for editing
            PolSecFileACL.SetAccessRuleProtection(True, True)

            'Set Rule to Remove unwanted access
            PolSecFileACL.SetAccessRule(New FileSystemAccessRule("NT AUTHORITY\SYSTEM", FileSystemRights.FullControl, AccessControlType.Allow))
            PolSecFileACL.SetAccessRule(New FileSystemAccessRule(LAdm, FileSystemRights.FullControl, AccessControlType.Allow))
            PolSecFileACL.RemoveAccessRuleAll(New FileSystemAccessRule("BUILTIN\Administrators", FileSystemRights.FullControl, AccessControlType.Allow))
            PolSecFileACL.RemoveAccessRuleAll(New FileSystemAccessRule("BUILTIN\Users", FileSystemRights.ReadAndExecute, AccessControlType.Allow))
            PolSecFileACL.RemoveAccessRuleAll(New FileSystemAccessRule(N3User, FileSystemRights.FullControl, AccessControlType.Allow))

            'Removes unwanted access
            File.SetAccessControl(Filename, PolSecFileACL)
        Catch ex As Exception
            LogFile.WriteEntry("Error setting reg permissions" & vbCrLf & ex.StackTrace & vbCrLf & vbCrLf &
                     ex.InnerException.ToString)
            VPN.Kill("Default")
        End Try
    End Sub

    Public Shared Sub CheckpointExeChk()
        'Checks to make sure the Checkpoint EXE has not been changed\renamed
        Try
            'Variable used for returned file rename
            Dim File As String

            'Checks to see if the Checkpoint EXE exists
            If System.IO.File.Exists(VPNPolling.CheckPDir & VPNPolling.ProcName & ".exe") = True Then
                'Passes found renamed Checkpoint EXE and creates it as a process to retrieve File description information
                Dim p As New Process
                p.StartInfo.FileName = VPNPolling.CheckPDir & VPNPolling.ProcName & ".exe"
                Dim FileInfoVersion As FileVersionInfo = FileVersionInfo.GetVersionInfo(p.StartInfo.FileName)

                'Checking that the checkpoint file found is the Security GUI
                If Not FileInfoVersion.FileDescription.Contains("Check Point Endpoint Security GUI") Then
                    File = CheckFileRename()
                    If Not File = Nothing Then
                        VPNPolling.ProblemIconAlert = True
                        VPN.Kill(File)
                    End If
                Else
                    Exit Sub
                End If
            Else
                File = CheckFileRename()
                If Not File = Nothing Then
                    VPNPolling.ProblemIconAlert = True
                    VPN.Kill(File)
                End If
            End If
        Catch ex As Exception
            LogFile.WriteEntry("Poller CheckpointExeChk Failed - " & ex.Message)
            LogFile.WriteEntry("Poller CheckpointExeChk Failed - " & ex.StackTrace)
        End Try
    End Sub

    Private Shared Function CheckFileRename()
        Try
            'CHECKFILERENAME - Goes through all EXE files in the Checkpoint area and find the name of the renamed EXE that has the process description
            'as Checkpoint Endpoint Security GUI

            Dim files() As String 'Array created to contain list of EXE files
            files = Directory.GetFiles(VPNPolling.CheckPDir, "*.exe", SearchOption.TopDirectoryOnly) 'Gets list of EXE files

            'Go through array of EXE files and find (If it exists) the renamed Checkpoint GUI
            For Each FileName As String In files
                Dim p As New Process
                p.StartInfo.FileName = FileName
                Dim FileInfoVersion As FileVersionInfo = FileVersionInfo.GetVersionInfo(p.StartInfo.FileName)
                If FileInfoVersion.FileDescription.Contains("Check Point Endpoint Security GUI") Then
                    Return FileName 'Once found return the file name of renamed EXE
                End If
            Next
            Return Nothing 'If rename not found then Checker is missing, unable to stop connections
        Catch ex As Exception
            LogFile.WriteEntry("Poller CheckFileRename Failed - " & ex.Message)
            LogFile.WriteEntry("Poller CheckFileRename Failed - " & ex.StackTrace)
            Return Nothing 'If rename not found then Checker is missing, unable to stop connections
        End Try
    End Function

    Public Shared Sub startSecurityScan()
        If Not PollingSecThread.IsAlive Then
            PollingSecThread.IsBackground = True
            PollingSecThread.Start()
        End If
    End Sub

    Public Shared Sub StopSecurityScan()
        If PollingSecThread.IsAlive Then PollingSecThread.Abort()
    End Sub

End Class
