﻿Imports System.Text.RegularExpressions

Public Class Tasks

    Public Shared Sub BckFreshCmd()
        RunCommand("schtasks", "/run /tn ""BGInfo Refresh""")
    End Sub

    Public Shared Sub VPNUpdaterCmd()
        RunCommand("schtasks", "/run /tn ""N3 VM Updater""")
    End Sub

    Public Shared Sub AutoUpdaterCmd()
        RunCommand("schtasks", "/run /tn ""ICNET Poller Autoupdate""")
    End Sub

    Public Shared Sub StartICNETCheckerCmd()
        RunCommand("schtasks", "/run /tn ""ICNET Start""")
    End Sub

    Public Shared Sub RunPackgMgr(SaveLoc As String)
        RunCommand("Pkgmgr", "/ip /m:" & SaveLoc & " /quiet")
    End Sub

    Public Shared Function RmvPackgMgrWUSA(KB As String)
        Return RunCommand("wusa.exe", "/uninstall /kb:" & KB & " /norestart")
    End Function

    Public Shared Function StartDISMUninstall()
        Return RunCommand(WindowsUpdatesUninstall.WUFulllistBatLoc, "")
    End Function

    Public Shared Function GetPkgListDISMDir()
        Return RunCommand(WindowsUpdatesUninstall.WUlistBatLoc, "")
    End Function

    '$SearchUpdates = dism /online /get-packages | findstr "Package_for"
    '$updates = $SearchUpdates.replace("Package Identity : ", "") | findstr "KBXXXXXX"
    'DISM.exe /Online /Remove-Package /PackageName:$updates /quiet /norestart

    Private Shared Function RunCommand(execute As String, arguments As String)
        LogFile.WriteEntry("$$$ - " & execute & " " & arguments)
        Using MyProcess As New Process
            MyProcess.StartInfo.UseShellExecute = False
            MyProcess.StartInfo.FileName = execute
            MyProcess.StartInfo.CreateNoWindow = True
            MyProcess.StartInfo.Arguments = arguments
            MyProcess.StartInfo.RedirectStandardOutput = True
            MyProcess.StartInfo.RedirectStandardError = True
            'If execute = "dism" Then MyProcess.StartInfo.Verb = "runas"
            MyProcess.EnableRaisingEvents = True
            MyProcess.Start()
            Dim cmdPid As Integer = MyProcess.Id
            Dim LogLine As String = Nothing
            MyProcess.WaitForExit()
            LogLine = Regex.Replace(MyProcess.StandardOutput.ReadToEnd, "\t\n\r", "")
            Dim ExitCode = MyProcess.ExitCode
            LogFile.WriteEntry("$$$ - ID:" & cmdPid.ToString & " - " & LogLine.Replace(vbLf, ""))
            LogFile.WriteEntry("$$$ - Exitcode=" & ExitCode.ToString)
            Return LogLine
        End Using
    End Function
End Class
