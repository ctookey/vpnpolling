﻿Imports System.IO
Imports System.Threading
Imports WUApiLib

Public Class WindowsUpdatesUninstall
    Inherits WindowsUpdates

    'Private Shared UpdatesToUninstall As New UpdateCollection
    Private Protected Shared RmvInstOffline As Boolean = False 'Used to determin what to rename file so processing is known if it was done offline or online for Seucirty Updates
    Private Protected Shared RmvInstFileExt As String = ".rmv" 'Used to retry updates process offline to being done online (.w10 .prc .tmp) for Security Updates
    Private Shared NewUpdates As IReadOnlyCollection(Of String)
    Private Shared KBUninstall As New List(Of String)
    Private Shared PkgList As New List(Of String)
    Public Shared ReadOnly WUFulllistBatLoc As String = PollerLoc & "\wu\dism-full-list.bat"
    Public Shared ReadOnly WUlistBatLoc As String = PollerLoc & "\wu\dism-list.bat"

    Private Shared WUUninstall As New Thread(New ThreadStart(AddressOf ListRemoveUpdates))

    Private Protected Shared Sub UninstallUpdate()
        If Directory.Exists(RmvUpDir) Then
            NewUpdates = My.Computer.FileSystem.GetFiles(RmvUpDir, FileIO.SearchOption.SearchTopLevelOnly, "*.rmv")
            'Counts the amount of files and only does this task if files are found
            If NewUpdates.Count > 0 Then
                Try
                    LogFile.WriteEntry("### - Windows security updates files found .rmv")
                    RmvInstFileExt = ".rmv"
                    'Start uninstalling Updates
                    WUUninstall = New Thread(New ThreadStart(AddressOf ListRemoveUpdates)) With {.IsBackground = True}
                    WUUninstall.Start()
                Catch ex As Exception
                    LogFile.WriteEntry("PollingService encountered an error - Removing updates .rmv'" & ex.Message & "'")
                    LogFile.WriteEntry("PollingService service stack trace: " & ex.StackTrace)
                    LogFile.WriteEntry("PollingService service stack values: " & WUUninstall.Name)
                    VPNPolling.ProblemIconAlert = True
                    VPN.Kill("Default")
                End Try
            Else
                LogFile.WriteEntry("### - Empty windows uninstall update folder - deleting folder " & RmvUpDir)
                Directory.Delete(RmvUpDir, True)
            End If
        End If
    End Sub

    Private Shared Sub ListRemoveUpdates()
        Try
            LogFile.WriteEntry("### - Windows Uninstall update - checking already installed components . . . .")
            Dim KB As String

            'Uninstalls security windows update
            LogFile.WriteEntry("### - Finding windows Updates to remove")
            Dim updateSession, updateSearcher
            updateSession = CreateObject("Microsoft.Update.Session")
            updateSearcher = updateSession.CreateUpdateSearcher()
            If Networking.CheckForInternetConnection() = False Then ' Checks if the internet is avilable to get updates
                LogFile.WriteEntry("### - Uninstall unable to get online to check update, running offline DB")
                RmvInstOffline = True
                updateSearcher.Online = False
            Else
                RmvInstOffline = False
                updateSearcher.Online = True
            End If

            Dim searchResult As Object = updateSearcher.Search("IsInstalled=1 and Type='Software' and IsHidden=0")
            ListInstalledUpdates(searchResult)

            'Remove file if update is not registered
            If KBUninstall.Count = 0 Then
                LogFile.WriteEntry("### - No Updates to uninstall, removing files")
                Directory.Delete(RmvUpDir, True)
                Exit Sub
            End If

            'now have a list of installed updates
            PkgList.Clear()
            LogFile.WriteEntry("### - Background collecting package full names started . . . .")
            Dim PkgListsStr As String = Tasks.GetPkgListDISMDir()
            If PkgListsStr IsNot Nothing Then
                Dim FullPkgList() = PkgListsStr.Split(vbCrLf)
                For Each PkgName In FullPkgList
                    If VPNPolling.PTrace = True Then LogFile.WriteEntry(PkgName)
                    If PkgName.Contains("Package_for") Then
                        For Each Uninstall In KBUninstall
                            Dim Result = GetPackageName(Uninstall, PkgName)
                            If VPNPolling.PTrace = True And Result IsNot Nothing Then LogFile.WriteEntry(Result)
                            If Result IsNot Nothing Then PkgList.Add("C:\Windows\system32\dism.exe /online /Remove-Package /PackageName:" & Result & " /quiet /norestart")
                        Next
                    End If
                Next
            End If

            'Start sending package information to uninstall
            If PkgList.Count > 0 Then
                LogFile.WriteEntry("### - Creating batch file for removal of updates")
                CreateBatch(WUFulllistBatLoc, PkgList)

                LogFile.WriteEntry("### - Running batch file for removal of updates")
                Tasks.StartDISMUninstall()

                'Find if uninstalled check this option!
                searchResult = updateSearcher.Search("IsInstalled=1 and Type='Software' and IsHidden=0")
                ListInstalledUpdates(searchResult)
            Else
                LogFile.WriteEntry("DISM Update not found, removing file")
                Directory.Delete(RmvUpDir, True)
                Exit Sub
            End If
            'Tasks.RmvPackageDISM()

            'Dim UninstallationResult = installer.Uninstall()
            'LogFile.WriteEntry(UpdatesToUninstall.Item().)

            'Output results of install
            'LogFile.WriteEntry("### - Removal result: " & UninstallationResult.ResultCode)
            'LogFile.WriteEntry("### - Reboot required: " & UninstallationResult.RebootRequired)
            'LogFile.WriteEntry("### - Listing of updates installed and individual installation results:")

            'Dim I As Integer
            'For I = 0 To UpdatesToUninstall.Count - 1
            '    'LogFile.WriteEntry("### -" & I + 1 & ": " & UpdatesToUninstall.Item(I).Title & ": " & UninstallationResult.GetUpdateResult(I).ResultCode)
            'Next

            'If File.Exists(KBFileName) Then
            '    LogFile.WriteEntry("### - Sec removing KB file as nolonger needed """ & KBFileName & """")
            '    File.Delete(KBFileName)
            '    'Create KB name to stop processing again
            '    If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Security", "KB" & hotfixid, Nothing) Is Nothing Then
            '        My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Security", "KB" & hotfixid, "Processed")
            '    End If
            'End If

        Catch ex As Exception
            LogFile.WriteEntry("### - Error Windows uninstall updates - " & ex.Message)
            LogFile.WriteEntry("### - Error Windows uninstall updates stack - " & ex.StackTrace)
        End Try
    End Sub

    Private Shared Sub CreateBatch(BatchFileName As String, BatchLines As List(Of String))
        Using w As StreamWriter = File.CreateText(BatchFileName)
            For Each Line In BatchLines
                w.WriteLine(Line)
            Next
        End Using
    End Sub

    Private Shared Function GetPackageName(KB As String, PkgName As String)
        If UCase(PkgName).Contains(UCase(KB)) Then
            Return PkgName.Replace("Package Identity : ", "").Replace(vbCrLf, "").Replace(vbCr, "").Replace(vbLf, "").Trim
        End If
        Return Nothing
    End Function

    Private Shared Function RmvCompletedFile(KBFileName As String)
        Dim KB As String = KBFileName.Replace(RmvInstFileExt, "").Substring(Len(RmvUpDir))
        If File.Exists(KBFileName) Then
            LogFile.WriteEntry("### - Sec removing KB file as nolonger needed """ & KBFileName & """")
            File.Delete(KBFileName)

            If Not File.Exists(KBFileName) Then
                'Create KB name to stop processing again
                If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Uninstall", KB, Nothing) Is Nothing Then
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Uninstall", KB, "Processed")
                End If

                Return True
            Else
                Return False
            End If
        End If
    End Function

    Private Shared Sub ListInstalledUpdates(searchResult As Object)
        KBUninstall.Clear()
        Dim update, kbarticleId
        'list the names of all files in the specified directory
        For Each KB In NewUpdates
            Dim KBNumber As String = KB.Replace(RmvInstFileExt, "").Substring(Len(RmvUpDir) + 3)
            If KB.Contains("KB") And KB.Contains(RmvInstFileExt) Then
                If VPNPolling.PTrace = True Then LogFile.WriteEntry("### - Updates installed " & CStr(searchResult.Updates.Count) & " found.")
                For index = 0 To searchResult.Updates.Count - 1
                    update = searchResult.Updates.Item(index)
                    Dim ResultCode = searchResult.ResultCode
                    For index2 = 0 To update.KBArticleIDs.Count - 1
                        kbarticleId = update.KBArticleIDs(index2)
                        If VPNPolling.PTrace = True Then
                            LogFile.WriteEntry("### - Found : " & kbarticleId & " looking for " & KBNumber)
                            LogFile.WriteEntry("### - ResultCode: " & ResultCode)
                        End If

                        If kbarticleId = KBNumber Then
                            If VPNPolling.PTrace = True Then LogFile.WriteEntry("### - Adding update to uninstall list: " & KBNumber)
                            KBUninstall.Add("KB" & KBNumber)
                        End If
                    Next
                Next
            Else
                LogFile.WriteEntry("File extension not recognised removing file")
                File.Delete(KB)
            End If
        Next
    End Sub

    Public Shared Sub StartUninstallingUpdates()
        UninstallUpdate()
    End Sub

    Public Shared Function getThreadState()
        If WUUninstall.ThreadState = ThreadState.Background Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Sub AbortThreads()
        If WUUninstall.IsAlive Then WUUninstall.Abort()
    End Sub
End Class
