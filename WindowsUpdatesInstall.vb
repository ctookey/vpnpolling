﻿Imports System.IO
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Threading
Imports WUApiLib

Public Class WindowsUpdatesInstall
    Inherits WindowsUpdates

    Private Protected Shared SecInstOffline As Boolean = False 'Used to determin what to rename file so processing is known if it was done offline or online for Seucirty Updates
    Private Protected Shared SecInstFileExt As String = ".w10" 'Used to retry updates process offline to being done online (.w10 .prc .tmp) for Security Updates

    'Used for downloads
    Private Protected Shared ReadOnly myCred As New System.Net.NetworkCredential("ngdemo", "NGd3mo12345678", "BAXICNET")
    Private Protected Const SecUpLoc As String = "https://downloads.icnetplc.com/N3-VM-Files/SecUpdates/"

    Private Protected Shared SecInstallThread As New Thread(New ThreadStart(AddressOf ChkUpdTOInst))
    Private Protected Shared SecOnlineInstallThread As New Thread(New ThreadStart(AddressOf StartSearchandInstall))

    Private Protected Shared Sub InstallUpdate()
        If Directory.Exists(SecUpDir) Then
            Dim NewUpdates = My.Computer.FileSystem.GetFiles(SecUpDir, FileIO.SearchOption.SearchTopLevelOnly, "*.w10")
            Dim ExtUpdates = My.Computer.FileSystem.GetFiles(SecUpDir, FileIO.SearchOption.SearchTopLevelOnly, "*.tmp")
            'Counts the amount of files and only does this task if files are found
            If NewUpdates.Count > 0 Then
                Try
                    LogFile.WriteEntry("### - Windows security updates files found w10")
                    SecInstFileExt = ".w10"
                    'Start installing Updates
                    SecInstallThread = New Thread(New ThreadStart(AddressOf ChkUpdTOInst)) With {.IsBackground = True}
                    SecInstallThread.Start()
                Catch ex As Exception
                    LogFile.WriteEntry("PollingService encountered an error - SEC updates w10'" & ex.Message & "'")
                    LogFile.WriteEntry("PollingService service stack trace: " & ex.StackTrace)
                    LogFile.WriteEntry("PollingService service stack values: " & SecInstallThread.Name)
                    VPNPolling.ProblemIconAlert = True
                    VPN.Kill("Default")
                End Try
            ElseIf ExtUpdates.Count > 0 Then
                If Networking.CheckForInternetConnection() Then
                    Try
                        LogFile.WriteEntry("### - Internet now available checking files again")
                        SecInstFileExt = ".tmp"
                        'Start Installing Updates
                        SecInstallThread = New Thread(New ThreadStart(AddressOf ChkUpdTOInst)) With {.IsBackground = True}
                        SecInstallThread.Start()
                    Catch ex As Exception
                        LogFile.WriteEntry("PollingService encountered an error - SEC updates TMP'" & ex.Message & "'")
                        LogFile.WriteEntry("PollingService service stack trace: " & ex.StackTrace)
                        LogFile.WriteEntry("PollingService service stack values: " & SecInstallThread.Name)
                        VPNPolling.ProblemIconAlert = True
                        VPN.Kill("Default")
                    End Try
                End If
            Else
                LogFile.WriteEntry("### - Empty windows security update folder - deleting folder " & SecUpDir)
                Directory.Delete(SecUpDir, True)
            End If
        End If
    End Sub

    Private Protected Shared Sub ChkUpdTOInst()
        Try
            LogFile.WriteEntry("### - Windows security update - checking already installed components . . . .")
            If Directory.Exists(SecUpDir) Then
                Dim di As New IO.DirectoryInfo(SecUpDir)
                Dim diar1 As IO.FileInfo() = di.GetFiles()
                Dim dra As IO.FileInfo

                'list the names of all files in the specified directory
                For Each dra In diar1
                    If dra.Name.Contains("KB") And dra.Name.Contains(SecInstFileExt) Then
                        ' Checks update file size as if it contains data then execute updates in this order
                        If dra.Length > 0 Then
                            LogFile.WriteEntry("### - Security update file contains update order information")
                            Dim fileList As String() = GetFileContents(dra.FullName)
                            If Not fileList.Length = 0 Then
                                Dim fileName As String = Nothing
                                For Each fileName In fileList
                                    OrderUpdateExecute(fileName, dra.Name)
                                Next
                            Else
                                LogFile.WriteEntry("### - File contains data but not able to read it or use it")
                            End If
                        Else
                            InstallCabFile(dra.Name)
                        End If
                    End If
                Next
            Else
                LogFile.WriteEntry("### - Windows security update install - nothing to do")
            End If
        Catch ex As Exception
            LogFile.WriteEntry("### - Error Windows security updates - " & ex.Message)
            LogFile.WriteEntry("### - Error Windows security updates stack - " & ex.StackTrace)
        End Try
    End Sub

    Private Protected Shared Sub InstallCabFile(cabName As String)
        Dim KBTmp As String = cabName.Replace(SecInstFileExt, "")
        LogFile.WriteEntry("### - Windows security update - " & KBTmp & " found")
        If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Security", KBTmp, Nothing) Is Nothing Or
                My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Security", KBTmp, Nothing) = "Once" Then
            KBTmp = KBTmp.Replace("KB", "")
            InstallSecUpdate(KBTmp)
        Else
            'Deleting file as this has been done already
            LogFile.WriteEntry("### - Skipping " & KBTmp & " as this security update has been done already")
            If File.Exists(SecUpDir & "\" & cabName) Then File.Delete(SecUpDir & "\" & cabName)
        End If
    End Sub

    Private Protected Shared Sub OrderUpdateExecute(fileList As String, KBName As String)
        Dim KBTmp As String = KBName.Replace(SecInstFileExt, "")
        If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Security", KBTmp, Nothing) Is Nothing Or
            My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Security", KBTmp, Nothing) = "Once" Then
            If fileList.Contains("SKIPVMMODEL:") = True Then
                Dim VMModel = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation\", "Model", "")
                Dim CPUModel = getSystem()
                LogFile.WriteEntry("### - Machine Architecture : " & CPUModel)
                If fileList.Contains(VMModel) Or fileList.Contains(CPUModel) Then
                    'Deleting file as this is not to be installed
                    LogFile.WriteEntry("### - Ignoring " & KBTmp & " as this security update is not to be installed on this platform")
                    If File.Exists(SecUpDir & "\" & KBName) Then File.Delete(SecUpDir & "\" & KBName)
                End If
            ElseIf fileList.Substring(fileList.Length - 4) = ".cab" Then
                LogFile.WriteEntry("### - " & fileList & " running CAB in order")
                InstallCabFile(fileList)
            Else
                LogFile.WriteEntry("### - " & fileList & " running OTHER in order")
                Dim SaveLoc = Nothing
                While True = True
                    SaveLoc = DwnSecUp(fileList, True)
                    If Not SaveLoc.Contains("Attempt") Then Exit While
                End While
                If File.Exists(SaveLoc) Then
                    LogFile.WriteEntry("### - Executing : " & SaveLoc)
                    Shell(SaveLoc, AppWinStyle.Hide, True, 60)
                End If
            End If
        Else
            'Deleting file as this has been done already
            LogFile.WriteEntry("### - Skipping " & KBTmp & " as this security update has been done already")
            If File.Exists(SecUpDir & "\" & KBName) Then File.Delete(SecUpDir & "\" & KBName)
        End If
    End Sub

    Private Protected Shared Function getSystem()
        Dim MyOBJ As Object
        Dim cpu As Object
        MyOBJ = GetObject("WinMgmts:").instancesof("Win32_Processor")
        For Each cpu In MyOBJ
            Return (cpu.Name.ToString)
        Next
    End Function

    Private Protected Shared Function GetFileContents(filename As String) As String()
        LogFile.WriteEntry("### - Reading security update file information : " & filename)
        Try
            'Open file in read only without file lock
            Dim fs As New FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
            Dim sr As New StreamReader(fs)
            Dim text = sr.ReadToEnd
            'Splits textstring into string array of lines
            Dim executeOrder() As String = text.Split({Environment.NewLine}, StringSplitOptions.None)
            fs.Close()
            Return executeOrder
        Catch
            LogFile.WriteEntry("### - Not able to read file : " & filename)
            Return Nothing
        End Try
    End Function

    Private Protected Shared Sub InstallSecUpdate(hotfixid As String)
        hotfixid = hotfixid.Replace(".cab", "")
        Dim KBFileName = "C:\N3-IT\SecUpdates\KB" & hotfixid & SecInstFileExt

        'Installs security windows update
        LogFile.WriteEntry("### - Finding windows sec Update : " & hotfixid)
        Dim updateSession, updateSearcher
        updateSession = CreateObject("Microsoft.Update.Session")
        updateSearcher = updateSession.CreateUpdateSearcher()
        If Networking.CheckForInternetConnection() = False Then ' Checks if the internet is avilable to get updates
            LogFile.WriteEntry("### - Sec unable to get online to check update, running offline DB")
            SecInstOffline = True
            updateSearcher.Online = False
        Else
            SecInstOffline = False
            updateSearcher.Online = True
        End If

        Dim searchResult
        searchResult = updateSearcher.Search("IsInstalled=1 and IsHidden=0")

        Dim update, kbArticleId, index, index2
        LogFile.WriteEntry("### - Sec " & CStr(searchResult.Updates.Count) & " found.")
        For index = 0 To searchResult.Updates.Count - 1
            update = searchResult.Updates.Item(index)
            For index2 = 0 To update.KBArticleIDs.Count - 1
                kbArticleId = update.KBArticleIDs(index2)
                LogFile.WriteEntry("### - Sec Found : " & kbArticleId)
                If kbArticleId = hotfixid Then
                    'Removing file to stop running again
                    If File.Exists(KBFileName) Then
                        LogFile.WriteEntry("### - Sec removing KB file as nolonger needed """ & KBFileName & """")
                        File.Delete(KBFileName)
                        'Create KB name to stop processing again
                        If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Security", "KB" & hotfixid, Nothing) Is Nothing Then
                            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Security", "KB" & hotfixid, "Processed")
                        End If
                    End If
                    Exit Sub
                End If
            Next
        Next

        'This gets run if the update is not found in it's search as it is required
        LogFile.WriteEntry("### - Downloading and installing Security update")
        'Downloading the file
        Dim SaveLoc As String = ""
        While True = True
            SaveLoc = DwnSecUp(hotfixid)
            If Not SaveLoc.Contains("Attempt") Then Exit While
        End While
        'Installing the security update
        LogFile.WriteEntry("### - Installing Security Update . . . .")
        Tasks.RunPackgMgr(SaveLoc)
        'Shell("Pkgmgr /ip /m:" & SaveLoc & " /quiet", AppWinStyle.Hide, True, 300)
        LogFile.WriteEntry("### - Installed Security Update . . . .")

        'Rename file as it's been processed
        If File.Exists(KBFileName) Then
            Dim KBNewName As String = "C:\N3-IT\SecUpdates\KB" & hotfixid & ".prc"
            If SecInstOffline = True Then
                KBNewName = "C:\N3-IT\SecUpdates\KB" & hotfixid & ".tmp"
            Else
                If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Security", "KB" & hotfixid, Nothing) Is Nothing Then
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Security", "KB" & hotfixid, "Once")
                ElseIf My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Security", "KB" & hotfixid, Nothing) = "Once" Then
                    My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\ICNet\CheckApp\WU\Security", "KB" & hotfixid, "Processed")
                End If
            End If

            If VPNPolling.PTrace = True Then LogFile.WriteEntry("### - Sec renaming file as it's been proccessed and not found on the system : """ & KBNewName & """")
            File.Move(KBFileName, KBNewName)
        End If
    End Sub

    Private Protected Shared Function DwnSecUp(hotfixid, Optional fileExt = False)
        ' If fileExt is provided then the download file is not a cabnet file and will have a different extenstion
        If fileExt = False And Not hotfixid.contains(".cab") Then
            hotfixid = "KB" & hotfixid & ".cab"
        ElseIf fileExt = False And hotfixid.contains(".cab") Then
            hotfixid = "KB" & hotfixid
        End If

        Try
            Dim filereader As New WebClient
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            filereader.Credentials = myCred
            LogFile.WriteEntry("### - Downloading " & hotfixid)
            filereader.DownloadFile(SecUpLoc & hotfixid, SecUpDir & "\" & hotfixid)
            filereader.Dispose()
            Return (SecUpDir & "\" & hotfixid)
        Catch ex As Exception
            LogFile.WriteEntry(ex.Message)
            If ex.Message.Contains("An unexpected error occurred on a send") Then
                LogFile.WriteEntry("### - Changing to use TLS 1.3")
                'Changing to use TLS 1.1
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls13
                Return ("AttemptSSL")
            ElseIf ex.Message.Contains("Could not establish trust relationship for the SSL/TLS secure channel") Then
                LogFile.WriteEntry("### - Changing Certificate CallBack")
                ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf CertificateValidationCallBack)
                Return ("AttemptCERT")
            Else
                Return ("Failed")
            End If
        End Try
    End Function

    Private Protected Shared Function CertificateValidationCallBack(
      ByVal sender As Object,
      ByVal certificate As X509Certificate,
      ByVal chain As X509Chain,
      ByVal sslPolicyErrors As SslPolicyErrors
    ) As Boolean
        Return True
    End Function

    Public Shared Sub startInstallingUpdates()
        InstallUpdate()
    End Sub

    Public Shared Function getThreadState()
        If SecInstallThread.ThreadState = ThreadState.Background Or SecOnlineInstallThread.ThreadState = ThreadState.Background Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Sub AbortThreads()
        If SecInstallThread.IsAlive Then SecInstallThread.Abort()
        If SecOnlineInstallThread.IsAlive Then SecOnlineInstallThread.Abort
    End Sub

    Public Shared Sub IntitialiseOnline()
        If Not SecOnlineInstallThread.IsAlive Then
            LogFile.WriteEntry("||| - Initalising forced full online update . . .")
            If Not WindowsUpdates.ChkReboot Then Networking.clrClientDisplayMessage()
            SecOnlineInstallThread = New Thread(New ThreadStart(AddressOf StartSearchandInstall)) With {.IsBackground = True}
            SecOnlineInstallThread.Start()
        Else
            LogFile.WriteEntry("||| - Still searching . . .")
        End If
    End Sub

    Private Shared Sub StartSearchandInstall()
        Try
            Dim mupdateSession As New WUApiLib.UpdateSession
            Dim updatesearcher As WUApiLib.IUpdateSearcher
            updatesearcher = mupdateSession.CreateUpdateSearcher()
            Dim SearchResult = getOnlineUpdates(mupdateSession, updateSearcher)
            If SearchResult IsNot Nothing Then installOnlineUpdates(mupdateSession, SearchResult)
        Catch ex As Exception
            LogFile.WriteEntry(ex.Message)
            LogFile.WriteEntry(ex.StackTrace)
        End Try
    End Sub

    Private Shared Function getOnlineUpdates(updateSession As UpdateSession, updateSearcher As IUpdateSearcher)
        Try
            'Download from MS the update required
            LogFile.WriteEntry("||| - Downloading forced updates . . .")
            Dim downloader As WUApiLib.UpdateDownloader = updateSession.CreateUpdateDownloader()
            LogFile.WriteEntry("||| - Creating collection . . .")
            downloader.Updates = New UpdateCollection()
            LogFile.WriteEntry("||| - Searching  . . .")
            Dim searchResult As ISearchResult = updateSearcher.Search("IsInstalled=0 and IsHidden=0 and DeploymentAction=*")
            Dim update As IUpdate
            LogFile.WriteEntry("||| - Installing upgrades only")

            For Each update In searchResult.Updates
                For Each category In update.Categories
                    If category.Name = "Upgrades" Then
                        LogFile.WriteEntry("||| - Adding update to collection :" & update.Title)
                        LogFile.WriteEntry("||| - " & category.Name & " {" & category.CategoryID & "}")
                        downloader.Updates.Add(update)
                    End If
                Next
            Next

            If downloader.Updates.Count > 0 Then
                LogFile.WriteEntry("||| - Starting download")

                ' downloader.Updates = UpdatesToDownload
                downloader.Download()
                LogFile.WriteEntry("||| - Finished download")
                Return searchResult
            Else
                LogFile.WriteEntry("||| - No forced updates found")
                Return Nothing
            End If
        Catch ex As Exception
            LogFile.WriteEntry(ex.Message)
            LogFile.WriteEntry(ex.StackTrace)
        End Try
        Return Nothing
    End Function

    Private Shared Sub installOnlineUpdates(updateSession As UpdateSession, SearchResult As ISearchResult)
        'install from MS the update required
        LogFile.WriteEntry("||| - Installing forced updates . . .")
        Dim updatesToInstall As New UpdateCollection()
        Dim update As IUpdate
        For Each update In SearchResult.Updates
            For Each category In update.Categories
                If category.Name = "Upgrades" Then updatesToInstall.Add(update)
            Next
        Next

        If updatesToInstall.Count = 0 Then
            LogFile.WriteEntry("||| - No forced updates need installing . . .")
            Exit Sub
        End If

        Dim installer As IUpdateInstaller = updateSession.CreateUpdateInstaller()
        installer.Updates = updatesToInstall
        Dim installationResult = installer.Install()

        'Output results of install
        LogFile.WriteEntry("||| - Installation Result: " & installationResult.ResultCode)
        LogFile.WriteEntry("||| - Reboot Required: " & installationResult.RebootRequired)
        If installationResult.RebootRequired Then Networking.setClientDisplayMessage("Windows System Upgrade : Reboot required." & vbCrLf & "Reboot now to continue.")
    End Sub

End Class
